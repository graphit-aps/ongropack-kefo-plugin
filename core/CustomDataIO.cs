﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace graphIT.Asprova.Plugin
{
    public class CustomDataIO
    {
        public string databaseTableName;
        public string fieldTrackChanges = "isTemp";
        public string fieldStatusFlag = "status_flag";
        public string fieldCreatedAt = "created_at";
        public string fieldLastUpdate = "last_update";

        private DataTable dt = new DataTable();
        public DataTable Dt
        {
            get { return dt; }
        }

        private MySqlDataAdapter adapterTarget;
        //private Type cls;
        private List<string> columns = new List<string>();
        private List<string> primaryKeys = new List<string>();
        private HashSet<string> firstPrimaryKeyValues = new HashSet<string>();
        private MySqlConnection conn;

        private Dictionary<string, object> objs = new Dictionary<string, object>();

        private Dictionary<string, FieldInfo> mapping = new Dictionary<string, FieldInfo>();


        public CustomDataIO(MySqlConnection conn, string databaseTableName, Type type = null)
        {
            this.conn = conn;
            this.databaseTableName = databaseTableName;

            //GetMapping(type);

            DownloadTargetData(false);
        }

        private void GetMapping(Type type)
        {
            // Get property array
            var properties = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);

            foreach (var p in properties)
            {
                mapping.Add(p.Name, p);
            }
        }

        /// <summary>
        /// Download data from the specified table.
        /// </summary>
        /// <param name="SchemaOnly"></param>
        public void DownloadTargetData(bool SchemaOnly = false)
        {
            string select = string.Format(@"SELECT * FROM {0}", databaseTableName);
            if (!SchemaOnly && firstPrimaryKeyValues.Count > 0)
            {
                // Download all data where the first primary key is a match, and any other which is not deleted (because if it is not active, than need to flag it as deleted)
                //select += string.Format($" WHERE {primaryKeys[0]} IN ('{string.Join("','", firstPrimaryKeyValues)}') OR ({fieldStatusFlag} is null OR {fieldStatusFlag} <> 'D')");
            }

            // Create table schema in C# and download records
            adapterTarget = new MySqlDataAdapter(string.Format(select, databaseTableName), conn);
            if (SchemaOnly)
            {
                // Download only schema
                adapterTarget.FillSchema(dt, SchemaType.Source);

                // Collect primary keys
                foreach (DataColumn col in dt.PrimaryKey)
                {
                    primaryKeys.Add(col.ColumnName);
                }

                // Collect columns
                foreach (DataColumn col in dt.Columns)
                {
                    columns.Add(col.ColumnName);
                }

                return;
            }

            // Download all data
            adapterTarget.Fill(dt);

            // Add virtual column for track changes
            //DataColumn column = new DataColumn(fieldTrackChanges, typeof(sbyte));
            //column.AllowDBNull = true;
            //dt.Columns.Add(column);
            adapterTarget.Dispose();
        }

        public void AddChild(object obj)
        {
            // Get the primary keys to use as an unique key in AsTable.
            var pks = new object[primaryKeys.Count];

            int iPk = 0;
            foreach (var c in primaryKeys)
            {
                var val = mapping[c].GetValue(obj);
                pks[iPk] = val;
                iPk++;
            }

            var pksString = String.Join("-", pks);

            // Use this object to this table.
            if (!firstPrimaryKeyValues.Contains(pksString))
            {
                firstPrimaryKeyValues.Add(pksString);
                objs.Add(pksString, obj);
            }
        }

        public void Upload()
        {
            DownloadTargetData();

            foreach (var obj in objs)
            {
                SaveInternal(obj.Value);
            }

            UploadTargetData();
        }

        private void UploadTargetData()
        {
            DataRow[] rows = null;
            var select = $"{fieldTrackChanges} is null AND({fieldStatusFlag} is null OR {fieldStatusFlag} <> 'D')";
            rows = dt.Select(select);

            foreach (var row in rows)
            {
                row.BeginEdit();

                // Mark rows if deleted
                row[fieldStatusFlag] = 'D';

                row.EndEdit();
            }

            // Remove helper column
            dt.Columns.Remove(fieldTrackChanges);

            // UPLOAD
            var cb = new MySqlCommandBuilder(adapterTarget);
            cb.ConflictOption = ConflictOption.CompareRowVersion;
            adapterTarget.Update(dt);

            dt.Clear();

            GC.Collect();
        }

        private void SaveInternal(object obj)
        {
            StatusFlag status = StatusFlag.None;

            DataRow row = null;

            var vals = new object[columns.Count];
            var pks = new object[primaryKeys.Count];


            int iVal = 0;
            int iPk = 0;
            foreach (var c in columns)
            {
                if (!mapping.ContainsKey(c))
                {
                    continue;
                    //new Exception("Missing variable");
                }

                var val = mapping[c].GetValue(obj);

                vals[iVal] = val;
                iVal++;

                if (primaryKeys.Contains(c))
                {
                    pks[iPk] = val;
                    iPk++;
                }
            }

            // Try to find it
            row = dt.Rows.Find(pks);
            if (row == null)
            {
                // Row not exists yet, create it
                status = StatusFlag.Added;
                row = dt.NewRow();
                row[fieldCreatedAt] = DateTime.Now;
                row[fieldLastUpdate] = DateTime.Now;
            }

            row.BeginEdit();

            // Update values
            iVal = 0;
            foreach (var c in columns)
            {
                SetRowValue(row, c, vals[iVal], status);
                iVal++;
            }

            // Set changed flag
            SetChangeFlag(row, status);

            row.EndEdit();

            // If new row, then add it to table
            if (status == StatusFlag.Added) dt.Rows.Add(row);
        }

        /// <summary>
        /// Set status flag
        /// </summary>
        /// <param name="status"></param>
        private void SetChangeFlag(DataRow row, StatusFlag status)
        {
            // Set that the row is exists.
            row[fieldTrackChanges] = 1;

            if (status == StatusFlag.Added || row[fieldStatusFlag].ToString() == "D")
            {
                row[fieldStatusFlag] = "A";
                row[fieldLastUpdate] = DateTime.Now;
                if (status == StatusFlag.Added && fieldCreatedAt != null) row[fieldCreatedAt] = DateTime.Now;
            }
            else if (status == StatusFlag.Modified && (row[fieldStatusFlag] == DBNull.Value || ((string)row[fieldStatusFlag]) != "A"))
            {
                // Only set to M when the status is not A
                row[fieldStatusFlag] = "M";
            }
        }

        /// <summary>
        /// Set a new value for the current row and change the status flag
        /// </summary>
        /// <param name="row">The current row</param>
        /// <param name="field">The field name of the value</param>
        /// <param name="value">The new value what we will set for the row</param>
        /// <param name="status">The current status flag of the row</param>
        private static void SetRowValue(DataRow row, string field, object value, StatusFlag status = StatusFlag.None)
        {
            value = value ?? DBNull.Value;
            if (!row[field].Equals(value))
            {
                row[field] = value;
                if (status == StatusFlag.None)
                {
                    status = StatusFlag.Modified;
                }
            }
        }

        private enum StatusFlag
        {
            None,
            Added,
            Modified,
            Deleted
        }
    }
}

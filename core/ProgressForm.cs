﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AspBosal.core
{
    public partial class ProgressForm : Form
    {

        public System.Timers.Timer tim = new System.Timers.Timer();


        public int OperationNr
        {
            get { return int.Parse(operationNr.Text); }
            set { operationNr.Text = value.ToString(); }
        }

        public int ProcessedOperationNr
        {
            get { return int.Parse(processedOperationNr.Text); }
            set { processedOperationNr.Text = value.ToString(); }
        }


        public ProgressForm()
        {
            InitializeComponent();
            operationNr.Text = Schedule.operationNr.ToString();
            processedOperationNr.Text = "0";
            IntPtr point = new IntPtr(Schedule.AsApp.app.HWnd);
            this.Parent = Control.FromHandle(point);
            tim.Interval = 100;
            tim.Elapsed += Tim_Elapsed1;
            tim.AutoReset = false;
            tim.Start();
        }

        private void Tim_Elapsed1(object sender, System.Timers.ElapsedEventArgs e)
        {
            Action act;

            act = () => processedOperationNr.Text = Schedule.processedOperationNr.ToString();
            processedOperationNr.Invoke(act);

            int oPct = (int)((((double)Schedule.processedOperationNr) / ((double)Schedule.operationNr)) * 100);
            act = () => operationProgress.Value = (int)oPct;
            operationProgress.Invoke(act);

            act = () => operationPercent.Text = oPct.ToString();
            operationPercent.Invoke(act);

            tim.Enabled = true;

            if (Schedule.processedOperationNr == Schedule.operationNr)
            {
                tim.Stop();
                tim.Enabled = false;
                this.DialogResult = DialogResult.OK;
            }

        }

        private void ProgressForm_Load(object sender, EventArgs e)
        {

        }
    }
}

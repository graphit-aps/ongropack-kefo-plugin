﻿using AsLib;
using AsPlugInManager;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

namespace graphIT.Asprova.Plugin
{
    public class Asprova : IDisposable
    {
        public ASPArgList args;
        public ASORootObject root;
        public ASBProject proj;
        public ASGWorkspace ws;

        public ASFApplication app;

        public Dictionary<string, ASOPropertyDef> properties = new Dictionary<string, ASOPropertyDef>();

        public string aslang;


        /// <summary>
        /// Constructor, initialize the Asprova default objects
        /// </summary>
        /// <param name="args">Asprova argument list</param>
        public Asprova(ASPArgList args)
        {
            this.args = args;
            root = (ASORootObject)args.ArgAsObject[TArgTypeAsObject.kArgRootObject];
            ws = (ASGWorkspace)args.ArgAsObject[TArgTypeAsObject.kArgWorkspace];
            app = (ASFApplication)args.ArgAsObject[TArgTypeAsObject.kArgApplication];
            proj = (ASBProject)args.ArgAsObject[TArgTypeAsObject.kArgProject];

            aslang = proj.FindChild("LocaleDef").Child[1].Code.Substring(0, 2);
        }

        /// <summary>
        /// Destructor
        /// </summary>
        public void Dispose()
        {

        }

        

        #region "Property"
        /// <summary>
        /// Gives back the Asprova property object from code. If the type is specified, the property will be created if not exists yet.
        /// </summary>
        /// <param name="code">Property's Asprova code</param>
        /// <param name="type">Asprova type</param>
        /// <returns></returns>
        public ASOPropertyDef GetPropertyDef(string code, TValueType? type = null)
        {
            if (!properties.ContainsKey(code))
            {
                ASOPropertyDef def = root.LookupPropertyDefFromCode(code);

                if (def == null && type != null)
                {
                    /* Property not exists yet, type specified, create now */
                    // Get class name
                    var className = code.Substring(0, code.IndexOf("_") - 1).Replace("User", "");

                    // create property
                    def = proj.CreateUserPropertyDefWithoutAddingToStyles(root.LookupClassDefFromCode(className), code, (TValueType)type, false);
                }

                properties.Add(code, def);
            }

            return properties[code];
        }

        public TPropertyID? GetPropertyId(string code)
        {
            var def = GetPropertyDef(code);

            return def == null ? null : (TPropertyID?)def.PropertyID;
        }

        /// <summary>
        /// Get multiple Asprova properties
        /// </summary>
        /// <param name="codes">Collection of properties. The dictionary value can be null, in that case the missing property not created</param>
        /// <returns></returns>
        public Dictionary<string, ASOPropertyDef> GetPropertyDefs(Dictionary<string, TValueType?> codes)
        {
            var res = new Dictionary<string, ASOPropertyDef>();

            foreach (KeyValuePair<string, TValueType?> pair in codes)
            {
                ASOPropertyDef tempDef = GetPropertyDef(pair.Key, pair.Value);

                if (tempDef == null) continue;

                res.Add(pair.Key, tempDef);
            }

            return res;
        }
        #endregion

        #region "Expression"
        public ASVExpression CreateExpression(string text)
        {
            ASVExpression expr = root.CreateExpression();
            expr.SetStr(text);

            return expr;
        }

        public ASVExpression CreateExpression(DateTime time)
        {
            ASVExpression expr = root.CreateExpression();
            expr.SetStr("#" + time.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture) + "#");

            return expr;
        }
        #endregion

        #region "Data IO"
        public bool DbioExecute(ASPArgList args, string type, string[] target)
        {
            try
            {
                ASIDBIO dbio = null;
                List<ASIDBIO> dbios = new List<ASIDBIO>();
                ASPCommandObject rootCommand = null;
                ASPExport cmdExport = null;
                ASPImport cmdImport = null;

                //Get root command
                rootCommand = proj.RootCommandObject;


                // Delete all previous command temp
                bool deleted = false;
                int i = 1;
                do
                {
                    deleted = false;
                    for (i = 1; i <= rootCommand.ChildCount; i++)
                    {
                        if (rootCommand.Child[i].Code == "CmdTemp")
                        {
                            rootCommand.DeleteChild(i);
                            deleted = true;
                            continue;
                        }
                    }
                } while (deleted);

                //Add command
                if (type.ToLower() == "export")
                    cmdExport = (ASPExport)rootCommand.AddChildAsCommand(TCmdClassID.kCmdASPExport, "CmdTemp");
                else
                    cmdImport = (ASPImport)rootCommand.AddChildAsCommand(TCmdClassID.kCmdASPImport, "CmdTemp");


                //Add dbio object to import command
                foreach (string code in target)
                {
                    dbio = (ASIDBIO)proj.RootDBIO.FindChild(code);
                    if (dbio == null)
                        continue;

                    dbios.Add(dbio);

                    if (type.ToLower() == "export")
                    {
                        cmdExport.Export_DBIO[0] = dbio;
                        dbio.IsSaveTable = TIsSaveTable.kIsSaveTableYes;
                    }
                    else
                    {
                        cmdImport.Import_DBIO[0] = dbio;
                        dbio.IsLoadTable = TIsLoadTable.kIsLoadTableYes;
                    }
                }

                //Execute command
                if (type.ToLower() == "export")
                    cmdExport.Execute((ASPCommandObject)cmdExport);
                else
                    cmdImport.Execute((ASPCommandObject)cmdImport);

                // Delete command
                for (i = 1; i <= rootCommand.ChildCount; i++)
                {
                    if (rootCommand.Child[i].Code == "CmdTemp")
                    {
                        rootCommand.DeleteChild(i);
                        break;
                    }
                }

                // Set the import and export to no
                foreach (var d in dbios)
                {
                    if (d == null)
                        continue;

                    d.IsSaveTable = TIsSaveTable.kIsSaveTableNo;
                    d.IsLoadTable = TIsLoadTable.kIsLoadTableNo;
                }

                // Check that there was any error
                foreach (var d in dbios)
                {
                    if (d == null)
                        continue;

                    if (d.ErrorCodeCount > 0)
                        return false;
                }

                dbios = null;
                dbio = null;
                rootCommand = null;
                cmdExport = null;
                cmdImport = null;

                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region "validation"

        public bool CheckPluginVersion(string version)
        {
            var prop = GetPropertyDef("ProjectUser_PluginVersion");
            if (prop == null)
            {
                MessageBox.Show("Plugin verzió tulajdonság nem található!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            var value = proj.GetAsStr((TPropertyID)prop.PropertyID, 0);

            if (value.ToString() != version.ToString())
            {
                //core.CreateLogEntry(string.Format("Plugin version problem ({0} - {1})", GetAspPropertyValue(args, "ProjectUser_PluginVersion"), version));
                MessageBox.Show($"Plugin verzió nem megfelelő {value.ToString()} - { version.ToString()}", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        public bool CheckPath()
        {
            var prop = GetPropertyDef("ProjectUser_MainFilePath");
            if (prop == null)
            {
                MessageBox.Show("Fájl útvonal verzió tulajdonság nem található!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            var mainpath = proj.GetAsStr((TPropertyID)prop.PropertyID, 0);

            string aspath = Utils.GetUNCPath(proj.RootObject.RootWorkspace.ProjectPathName + "\\" + proj.RootObject.RootWorkspace.ProjectFilename);

            if (mainpath.ToLower() != aspath.ToLower())
            {
                //core.CreateLogEntry("Prototype (" + aspath.ToLower + ") is not the main prototype (" + mainpath.ToLower + ")!");
                MessageBox.Show($"A jelenleg megnyitott prototípus ({aspath.ToLower()}) nem a fő prototípus ({mainpath.ToLower()})!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }


        public bool CheckLicense()
        {
            bool res = false;

            if (app.IsValidLicense)
                res = true;
            else
            {
                //core.CreateLogEntry("License problem");
                MessageBox.Show("Licenc probléma", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return res;
        }
        #endregion

    }
}

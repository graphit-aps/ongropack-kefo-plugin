﻿using AsLib;
using AsPlugInManager;
//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using static AsPlugInManager.TArgTypeAsObject;
using static AsPlugInManager.TArgTypeAsString;
using static AsPlugInManager.TReturnType;

namespace graphIT.Asprova.Plugin
{
    public static class Commands
    {
        #region "Multi property"
        //private static List<string> rows;
        private static Dictionary<string, ASOObject> objs;
        private static ASOObject table;
        private static int propMultiObject;
        private static int propMultiValue;
        private static int propMultiIndex;


        public static void CommandMultiPropertyValues3(IASPArgList argList, string asprovaClass, List<string> definition)
        {
            ASPArgList args = (ASPArgList)argList;
            ASBProjectEx asproj = (ASBProjectEx)args.ArgAsObject[kArgProject];
            ASORootObject asroot = (ASORootObject)args.ArgAsObject[kArgRootObject];
            ASOObjectList objList = asroot.CreateObjectList();

            objs = new Dictionary<string, ASOObject>();

            table = asproj.FindChild(asprovaClass);
            if (table == null) return;
            propMultiValue = asroot.LookupPropID($"{asprovaClass}User_PrevOperation");
            if (propMultiValue == 0) return;


            // Collect current records
            var count = table.ChildCount;
            for (int i = 1; i <= count; i++)
            {
                var obj = table.Child[i];
                objs.Add($"{obj.Code}-{obj.GetAsStr((TPropertyID)propMultiValue, 1)}",obj);

                //objList.AddObject(0, table.Child[i]);
            }
            //asproj.DeleteObjects(objList);
            //objList.Clear();


            // Iterate properties
            foreach (var propertyCode in definition)
            {
                if (!propertyCode.Contains("_")) continue;

                var tableCode = propertyCode.Substring(0, propertyCode.IndexOf("_")).TrimEnd("User");

                ASOObject table = asproj.FindChild(tableCode);
                if (table == null)
                {
                    switch (tableCode)
                    {
                        case "Work":
                            objList = asproj.GetOperationList(true);
                            break;

                        default:
                            continue;
                    }
                }

                int propertyId = -1;

                switch (propertyCode)
                {
                    case "WorkUser_PrevOperationRecursive":
                    case "WorkUser_NextOperationRecursive":
                        break;
                    default:
                        propertyId = asroot.LookupPropID(propertyCode);
                        break;
                }

                if (propertyId == 0) continue;

                // Iterate rows
                if (table != null)
                {
                    for (int i = 1; i <= table.ChildCount; i++)
                    {
                        GetObjectProperties3(asroot, table.Child[i], propertyCode, propertyId);
                    }
                }
                else
                {
                    for (int i = 1; i <= objList.ObjectCount; i++)
                    {
                        GetObjectProperties3(asroot, objList.Object[i], propertyCode, propertyId);
                    }
                }
            }

            objList.Clear();

            foreach (var kvp in objs)
            {
                objList.AddObject(0,kvp.Value);
            }
            asproj.DeleteObjects(objList);

            objs.Clear();
            objs = null;

            table = null;

            objList.Clear();
            objList = null;

            asproj.BroadcastChanges();

            asroot = null;
            asproj = null;
            args = null;
        }

        private static void GetObjectProperties3(ASORootObject asroot, ASOObject obj, string propertyCode, int propertyId)
        {
            int i = 1;

            var objectString = obj.Code;

            var values = new List<string>();

            if (propertyId != -1)
            {
                for (i = 1; i <= asroot.PropertyValueCount(obj, propertyId); i++)
                {
                    values.Add(obj.GetAsStr((TPropertyID)propertyId, i));
                }
            }
            else
            {
                switch (propertyCode)
                {
                    case "WorkUser_PrevOperationRecursive":
                        values = GetPreviousOperationsRecursive((ASBOperationEx)obj);
                        break;
                    case "WorkUser_NextOperationRecursive":
                        values = GetNextOperationsRecursive((ASBOperationEx)obj);
                        break;
                    default:
                        throw new Exception($"Not handled propertyCode {propertyCode}!");
                }
            }


            // Iterate row's property index
            i = 1;
            foreach (var value in values)
            {
                if (objs.ContainsKey($"{objectString}-{value}"))
                {
                    objs.Remove($"{objectString}-{value}");
                    continue;
                }

                var newRow = table.AddChild(table.ClassID, 0, objectString);
                //newRow.SetAsStr((TPropertyID)propMultiObject, 1, objectString);
                newRow.SetAsStr((TPropertyID)propMultiValue, 1, value);
                //newRow.SetAsStr((TPropertyID)propMultiIndex, 1, i.ToString());

                i++;
            }
        }

        public static void CommandMultiPropertyValues(IASPArgList argList, List<string> definition)
        {
            ASPArgList args = (ASPArgList)argList;
            ASBProjectEx asproj = (ASBProjectEx)args.ArgAsObject[kArgProject];
            ASORootObject asroot = (ASORootObject)args.ArgAsObject[kArgRootObject];
            ASOObjectList objList = asroot.CreateObjectList();

            table = asproj.FindChild("Multi");
            if (table == null) return;
            propMultiObject = asroot.LookupPropID("MultiUser_Object");
            if (propMultiObject == 0) return;
            propMultiValue = asroot.LookupPropID("MultiUser_Value");
            if (propMultiValue == 0) return;
            propMultiIndex = asroot.LookupPropID("MultiUser_Index");
            if (propMultiIndex == 0) return;


            // Collect current records
            var count = table.ChildCount;
            for (int i = 1; i <= count; i++)
            {
                objList.AddObject(0, table.Child[i]);
            }
            asproj.DeleteObjects(objList);
            objList.Clear();


            // Iterate properties
            foreach (var propertyCode in definition)
            {
                if (!propertyCode.Contains("_")) continue;

                var tableCode = propertyCode.Substring(0, propertyCode.IndexOf("_")).TrimEnd("User");

                ASOObject table = asproj.FindChild(tableCode);
                if (table == null)
                {
                    switch (tableCode)
                    {
                        case "Work":
                            objList = asproj.GetOperationList(true);
                            break;

                        default:
                            continue;
                    }
                }

                int propertyId = -1;

                switch (propertyCode)
                {
                    case "WorkUser_PrevOperationRecursive": 
                    case "WorkUser_NextOperationRecursive":
                        break;
                    default:
                        propertyId = asroot.LookupPropID(propertyCode);
                        break;
                }
                
                if (propertyId == 0) continue;

                // Iterate rows
                if (table != null)
                {
                    for (int i = 1; i <= table.ChildCount; i++)
                    {
                        GetObjectProperties(asroot, table.Child[i], propertyCode, propertyId);
                    }
                }
                else
                {
                    for (int i = 1; i <= objList.ObjectCount; i++)
                    {
                        GetObjectProperties(asroot, objList.Object[i], propertyCode, propertyId);
                    }
                }
            }

            table = null;

            objList.Clear();
            objList = null;

            asproj.BroadcastChanges();

            asroot = null;
            asproj = null;
            args = null;
        }

        private static void GetObjectProperties(ASORootObject asroot, ASOObject obj, string propertyCode, int propertyId)
        {
            int i = 1;

            var objectString = obj.Code;

            var values = new List<string>();

            if (propertyId != -1)
            {
                for (i = 1; i <= asroot.PropertyValueCount(obj, propertyId); i++)
                {
                    values.Add(obj.GetAsStr((TPropertyID)propertyId, i));
                }
            }
            else
            {
                switch (propertyCode)
                {
                    case "WorkUser_PrevOperationRecursive":
                        values = GetPreviousOperationsRecursive((ASBOperationEx)obj);
                        break;
                    case "WorkUser_NextOperationRecursive":
                        values = GetNextOperationsRecursive((ASBOperationEx)obj);
                        break;
                    default:
                        throw new Exception($"Not handled propertyCode {propertyCode}!");
                }
            }

            
            // Iterate row's property index
            i = 1;
            foreach (var value in values)
            {
                var newRow = table.AddChild(table.ClassID, 0, propertyCode);
                newRow.SetAsStr((TPropertyID)propMultiObject, 1, objectString);
                newRow.SetAsStr((TPropertyID)propMultiValue, 1, value);
                newRow.SetAsStr((TPropertyID)propMultiIndex, 1, i.ToString());

                i++;
            }
        }
        #endregion

        #region "Operations recursive"


        private static List<string> GetPreviousOperationsRecursive(ASBOperationEx operation)
        {
            var opsCode = new List<string>();

            GetPrevOperations(operation, opsCode);

            return opsCode;
        }

        private static void GetPrevOperations(ASBOperationEx op, List<string> opsCode)
        {
            int count = op.PrevOperationCount;

            for (int i = 1; i <= count; i++)
            {
                var currOp = (ASBOperationEx)op.PrevOperation[i];

                if (opsCode.Contains(currOp.Code)) continue;

                opsCode.Add(currOp.Code);

                GetPrevOperations(currOp, opsCode);
            }
        }

        private static List<string> GetNextOperationsRecursive(ASBOperationEx operation)
        {
            var opsCode = new List<string>();

            GetNextOperations(operation, opsCode);

            return opsCode;
        }

        private static void GetNextOperations(ASBOperationEx op, List<string> opsCode)
        {
            int count = op.NextOperationCount;

            for (int i = 1; i <= count; i++)
            {
                var currOp = (ASBOperationEx)op.NextOperation[i];

                if (opsCode.Contains(currOp.Code)) continue;

                opsCode.Add(currOp.Code);

                GetNextOperations(currOp, opsCode);
            }
        }
        #endregion

        

        /*public static void CommandMultiPropertyValues2(IASPArgList argList, List<string> definition)
        {
            var conn = new MySqlConnection("Server=localhost;Database=bosal_asprova;Uid=root;Pwd=infodba@56");
            conn.Open();
            var transaction = conn.BeginTransaction();

            ASPArgList args = (ASPArgList)argList;
            ASBProjectEx asproj = (ASBProjectEx)args.ArgAsObject[kArgProject];
            ASORootObject asroot = (ASORootObject)args.ArgAsObject[kArgRootObject];
            ASOObjectList objList = asroot.CreateObjectList();

            var dio = new CustomDataIO(conn, "asp_multi", typeof(Multi));

            table = asproj.FindChild("Multi");
            if (table == null) return;
            propMultiObject = asroot.LookupPropID("MultiUser_Object");
            if (propMultiObject == 0) return;
            propMultiValue = asroot.LookupPropID("MultiUser_Value");
            if (propMultiValue == 0) return;
            propMultiIndex = asroot.LookupPropID("MultiUser_Index");
            if (propMultiIndex == 0) return;


            // Iterate properties
            foreach (var propertyCode in definition)
            {
                if (!propertyCode.Contains("_")) continue;

                var tableCode = propertyCode.Substring(0, propertyCode.IndexOf("_")).TrimEnd("User");

                ASOObject table = asproj.FindChild(tableCode);
                if (table == null)
                {
                    switch (tableCode)
                    {
                        case "Work":
                            objList = asproj.GetOperationList(true);
                            break;

                        default:
                            continue;
                    }
                }

                int propertyId = -1;

                switch (propertyCode)
                {
                    case "WorkUser_PrevOperationRecursive":
                    case "WorkUser_NextOperationRecursive":
                        break;
                    default:
                        propertyId = asroot.LookupPropID(propertyCode);
                        break;
                }

                if (propertyId == 0) continue;

                // Iterate rows
                if (table != null)
                {
                    for (int i = 1; i <= table.ChildCount; i++)
                    {
                        GetObjectProperties2(dio, asroot, table.Child[i], propertyCode, propertyId);
                    }
                }
                else
                {
                    for (int i = 1; i <= objList.ObjectCount; i++)
                    {
                        GetObjectProperties2(dio, asroot, objList.Object[i], propertyCode, propertyId);
                    }
                }
            }

            dio.Upload();
            dio = null;

            objList.Clear();
            objList = null;

            asproj.BroadcastChanges();

            asroot = null;
            asproj = null;
            args = null;


            transaction.Commit();

            conn.Close();
            conn.Dispose();
            conn = null;
        }*/

        /*private static void GetObjectProperties2(CustomDataIO dio, ASORootObject asroot, ASOObject obj, string propertyCode, int propertyId)
        {
            int i = 1;

            var objectString = obj.Code;

            var values = new List<string>();

            if (propertyId != -1)
            {
                for (i = 1; i <= asroot.PropertyValueCount(obj, propertyId); i++)
                {
                    values.Add(obj.GetAsStr((TPropertyID)propertyId, i));
                }
            }
            else
            {
                switch (propertyCode)
                {
                    case "WorkUser_PrevOperationRecursive":
                        values = GetPreviousOperationsRecursive((ASBOperationEx)obj);
                        break;
                    case "WorkUser_NextOperationRecursive":
                        values = GetNextOperationsRecursive((ASBOperationEx)obj);
                        break;
                    default:
                        throw new Exception($"Not handled propertyCode {propertyCode}!");
                }
            }


            // Iterate row's property index
            i = 1;
            foreach (var value in values)
            {
                dio.AddChild(new Multi() { property = propertyCode, obj = objectString, value = value, index = i.ToString() });

                i++;
            }
        }*/


        public static string TrimEnd(this string source, string value)
        {
            if (!source.EndsWith(value))
                return source;

            return source.Remove(source.LastIndexOf(value));
        }
    }

    public class Multi
    {
        public string property;
        public string obj;
        public string value;
        public string index;

        public Multi()
        {

        }
    }
}

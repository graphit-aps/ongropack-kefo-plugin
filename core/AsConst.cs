﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace graphIT.Asprova.Plugin
{
    public static class AsCont
    {
        public abstract class MyEnum
        {
            public object Val { get; set; }
        }

        [DebuggerDisplay("{DebuggerDisplay,nq}")]
        public abstract class MyEnum<T> : MyEnum
        {
            public MyEnum(T value, string name)
            {
                Name = name;
                Value = value;
                Val = value;
            }

            public string Name { get; set; }

            public T Value { get; set; }

            public override bool Equals(object obj)
            {
                return obj is MyEnum<T> && this == (MyEnum<T>)obj;
            }

            public override int GetHashCode()
            {
                return Val.GetHashCode();
            }

            public static bool operator ==(MyEnum<T> x, MyEnum<T> y)
            {
                return (ReferenceEquals(x, null) && ReferenceEquals(y, null)) || (!ReferenceEquals(x, null) && !ReferenceEquals(y, null) && Equals(x.Val, y.Val));
            }
            public static bool operator !=(MyEnum<T> x, MyEnum<T> y)
            {
                return (!(x == y));
            }

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private string DebuggerDisplay
            {
                get { return Name; }
            }
        }

        public class Direction : MyEnum<string>
        {
            private Direction(string value, string name) : base(value, name) { }

            public static Direction Forward { get { return new Direction("F", "Forward"); } }

            public static Direction Backward { get { return new Direction("B", "Backward"); } }
        }
    }
}

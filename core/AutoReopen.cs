﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsLib;
using AsPlugInManager;
using System.Windows.Forms;
using System.IO;

namespace graphIT.Asprova.Plugin
{
    public static class AutoReopen
    {
        public static bool AutoReopenNeccesary = false;
        public static bool Disabled = false;

        public static TReturnType CheckReopenNeed2(IASPArgList argList)
        {
            ASFApplication app = (ASFApplication) argList.ArgAsObject[TArgTypeAsObject.kArgApplication];
            if (app.AsprovaModuleType == TAsprovaModuleType.kAsprovaModuleType_BOM || app.AsprovaModuleType == TAsprovaModuleType.kAsprovaModuleType_MES)
                AutoReopenNeccesary = true;
            else AutoReopenNeccesary = false;

            app = null;
            GC.Collect();

            return TReturnType.kContinue;
        }

        public static TReturnType SerializeCheck2(IASPArgList argList)
        {
            if (!AutoReopenNeccesary) return TReturnType.kContinue;

            MessageBox.Show("A prototípus mentése nem engedélyezett ezzel a hozzáféréssel!", "Mentés nem engedélyezett!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return TReturnType.kBreak;
        }

        public static TReturnType CheckNewVersion2(IASPArgList argList)
        {
            try
            {
                if (Disabled || !AutoReopenNeccesary) return TReturnType.kContinue;
                Disabled = true;

                ASGWorkspace ws = (ASGWorkspace)argList.ArgAsObject[TArgTypeAsObject.kArgWorkspace];
                ASBProject proj = (ASBProject)argList.ArgAsObject[TArgTypeAsObject.kArgProject];
                string path = Common.GetUNCPath(ws.ProjectPathName + "\\" + ws.ProjectFilename);
                DateTime startup = proj.StartupTime;

                ws = null;
                proj = null;
                GC.Collect();

                if (File.Exists(path))
                {
                    DateTime lastMod = File.GetLastWriteTime(path);

                    if (lastMod > startup)
                    {
                        MessageBox.Show("Újabb verzió érhető el az Asprova prototípusból. A megnyitott file automatikusan frissül!", "Asprova prototípus frissítése", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ASFDocument doc = (ASFDocument)argList.ArgAsObject[TArgTypeAsObject.kArgDocument];
                        doc.Revert();
                        doc = null;
                        GC.Collect();
                    }

                }

                Disabled = false;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Disabled = false;
                return TReturnType.kError;
            }

            return TReturnType.kContinue;

        }



    }
}

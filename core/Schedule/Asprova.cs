﻿using AsLib;
using AsPlugInManager;
using System;
using System.Collections.Generic;
using System.Globalization;
using static graphIT.Asprova.Plugin.AsCont;
using static graphIT.Utils;

namespace graphIT.Asprova.Plugin.Schedule
{
    public class Asprova : Plugin.Asprova, IDisposable
    {
        public ASPCommandObject cmd;
        public ASPSchedulingParameter cmdParent = null;
        

        public CmdParam cmdParam { get; set; } = new CmdParam();

        public ASLAssigner FwAssigner = null;
        public ASLAssigner BwAssigner = null;


        /// <summary>
        /// Constructor, initialize the Asprova default objects
        /// </summary>
        /// <param name="args">Asprova argument list</param>
        public Asprova(ASPArgList args) : base(args)
        {
            cmd = (ASPCommandObject)args.ArgAsObject[TArgTypeAsObject.kArgCommandObject];
            if (cmd != null && cmd.Parent.Code != "Scheduling") cmdParent = (ASPSchedulingParameter)cmd.Parent;
            if (cmdParent != null)
            {
                var asprop = GetPropertyId("DefaultSchedulingParameterUser_API_AssignParameter");
                if (asprop != null)
                {
                    cmdParam.Update(cmdParent.GetAsStr((TPropertyID)asprop, 1));
                }
            }
            

            CreateAssigner();
            

        }

        private void CreateAssigner()
        {
            if (FwAssigner != null) Release(FwAssigner);
            if (BwAssigner != null) Release(FwAssigner);

            FwAssigner = root.CreateAssigner();
            BwAssigner = root.CreateAssigner();

            // Clone the parameters from the parent scheduling parameter
            FwAssigner.Parameter.AssignmentType = cmdParent.AssignmentType;
            FwAssigner.Parameter.AssignmentDirection = TAssignmentDirection.kCmdAssignmentDirectionForward;
            FwAssigner.Parameter.ResSelectionType = cmdParent.ResSelectionType;
            ((ASPSchedulingParameter)FwAssigner.Parameter).IgnoreUnassignedPeggedOperations = cmdParent.IgnoreUnassignedPeggedOperations;
            FwAssigner.Parameter.UseCombinationSetup = cmdParent.UseCombinationSetup;

            BwAssigner.Parameter.AssignmentType = cmdParent.AssignmentType;
            BwAssigner.Parameter.AssignmentDirection = TAssignmentDirection.kCmdAssignmentDirectionBackward;
            BwAssigner.Parameter.ResSelectionType = cmdParent.ResSelectionType;
            ((ASPSchedulingParameter)BwAssigner.Parameter).IgnoreUnassignedPeggedOperations = cmdParent.IgnoreUnassignedPeggedOperations;
            BwAssigner.Parameter.UseCombinationSetup = cmdParent.UseCombinationSetup;

            // Resource evaluation
            for (int i = 1; i <= cmdParent.ResEvalCount; i++)
            {
                if (cmdParent.ResEval[i].Code.Contains("Backward"))
                {
                    BwAssigner.Parameter.ResEval[BwAssigner.Parameter.ResEvalCount + 1] = cmdParent.ResEval[i];
                }
                else if (cmdParent.ResEval[i].Code.Contains("Forward"))
                {
                    FwAssigner.Parameter.ResEval[FwAssigner.Parameter.ResEvalCount + 1] = cmdParent.ResEval[i];
                }
                else
                {
                    BwAssigner.Parameter.ResEval[BwAssigner.Parameter.ResEvalCount + 1] = cmdParent.ResEval[i];
                    FwAssigner.Parameter.ResEval[FwAssigner.Parameter.ResEvalCount + 1] = cmdParent.ResEval[i];
                }
            }
        }

        /// <summary>
        /// Destructor
        /// </summary>
        new public void Dispose()
        {
            Release(FwAssigner);
            Release(BwAssigner);

            base.Dispose();
        }

        #region "Command Parameter"
        public class CmdParam
        {
            public List<string> Flags { get; set; } = new List<string>();
            public Dictionary<string, string> Values { get; set; } = new Dictionary<string, string>();

            public CmdParam()
            {
            }

            public CmdParam(string input)
            {
                Update(input);
            }

            public void Update(string input)
            {
                input = input.Trim();

                foreach (string value in input.Split(';'))
                {
                    if (value.Contains("="))
                    {
                        var values = value.Split('=');
                        if (!Values.ContainsKey(values[0])) Values.Add(values[0], values[1]);
                    }
                    else
                    {
                        Flags.Add(value);
                    }
                }
            }
        }
        #endregion

        #region "Assign"
        public bool Assign(ASBOperationEx operation, Direction direction = null, DateTime? time = null)
        {
            bool success = false;
            bool retry = false;

            var assigner = direction.Value == Direction.Backward.Value ? BwAssigner : FwAssigner;

            for (int i = 1; i <= 2; i++)
            {
                // Set the reference time for the command
                if (time == null)
                {
                    time = operation.TotalCalculatedEST;

                    if (direction.Value == Direction.Backward.Value)
                    {
                        time = operation.TotalCalculatedLET;

                        var length = operation.NextOperationCount;
                        for (int j = 1; j <= length; j++)
                        {
                            if (operation.Order.Code == operation.NextOperation[j].Order.Code && operation.NextOperation[j].IsAssigned != TIsAssigned.kIsAssignedUnassigned)
                            {
                                time = proj.SchedulingEndTimeAsDATE;
                                break;
                            }
                        }
                    }
                }

                assigner.Parameter.AssignmentStartTime = (direction.Value == Direction.Forward.Value ? CreateExpression((DateTime)time) : null);
                assigner.Parameter.AssignmentEndTime = (direction.Value == Direction.Forward.Value ? null : CreateExpression((DateTime)time));

                success = assigner.AssignOperation((ASBOperation)operation, (DateTime)time);

                if (!success)
                    break;

                /* SS timeconstraint */
                if (direction.Value == Direction.Backward.Value)
                {
                    for (int j = 1; j <= operation.NextOperationCount; j++)
                    {
                        var nextOp = operation.NextOperation[j];

                        // If the next operation is not assigned yet, then exit
                        if (operation.NextOperation[j].IsAssigned != TIsAssigned.kIsAssignedAssigned) continue;

                        // Next operation has EST violation?
                        if (operation.NextOperation[j].StartTime < operation.NextOperation[j].TotalCalculatedEST)
                        {
                            time = operation.EndTime - operation.NextOperation[j].TotalCalculatedEST.Subtract(operation.NextOperation[j].StartTime);
                            retry = true;
                            break;
                        }
                    }
                }

                if (!retry)
                    break;
            }

            return success;
        }
        #endregion
    }
}

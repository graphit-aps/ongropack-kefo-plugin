﻿using AsPlugInManager;
using System;
using System.Windows.Forms;
using static AsPlugInManager.TReturnType;

namespace graphIT.Asprova.Plugin.Schedule
{
    public class Schedule
    {
        public TReturnType Scheduling(ASPArgList argList)
        {
            Asprova asprova = null;
            ScheduleData data = null;
            ScheduleLogic scheduling = null;

            try
            {
                asprova = new Asprova(argList);
                data = new ScheduleData(asprova);
                scheduling = new ScheduleLogic(asprova, data);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Hozzárendelés hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return kBreak;
            }
            finally
            {
                if (scheduling != null)
                {
                    scheduling.Dispose();
                    scheduling = null;
                }
                    
                if (data != null)
                {
                    data.Dispose();
                    data = null;
                }
                    
                if (asprova != null)
                {
                    asprova.Dispose();
                    asprova = null;
                }  
            }

            return kContinue;
        }
    }
}

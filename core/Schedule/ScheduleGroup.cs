﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static graphIT.Asprova.Plugin.AsCont;

namespace graphIT.Asprova.Plugin.Schedule
{
    [DebuggerDisplay("{Id}")]
    public class Group
    {
        public Dictionary<int, Group> parent;
        public int Id { get; set; }
        public Dictionary<string, ScheduleOperation> Operations = new Dictionary<string, ScheduleOperation>();

        public ScheduleOperation DefaultFirstOp { get; set; }

        public int MinDispatchingOrder = 0;
        public int MaxDispatchingOrder = 0;

        public DateTime MinimumLET = DateTime.MaxValue;

        public Dictionary<string, int> DispatchingOrder = new Dictionary<string, int>();
        public Dictionary<string, int> AssignmentOrder = new Dictionary<string, int>();

        public Dictionary<int, ScheduleOperation> ForwardOrder = new Dictionary<int, ScheduleOperation>();

        Direction _directionOriginal;
        public Direction DirectionOriginal
        {
            get
            {
                return _directionOriginal;
            }
            set
            {
                _directionOriginal = value;
            }
        }

        public string DirectionCurrent { get; set; } = null;

        public Group(Dictionary<int, Group> parent, int id)
        {
            this.parent = parent;
            Id = id;
        }

        public void AddOperation(ScheduleOperation operation)
        {
            if (!Operations.ContainsKey(operation.Code))
            {
                if (MinDispatchingOrder == 0 || MinDispatchingOrder > operation.DefaultAssignmentOrder)
                {
                    DefaultFirstOp = operation;
                    MinDispatchingOrder = operation.DefaultAssignmentOrder;
                }

                if (MaxDispatchingOrder == 0 || MaxDispatchingOrder < operation.DefaultAssignmentOrder)
                {
                    MaxDispatchingOrder = operation.DefaultAssignmentOrder;
                }

                if (operation.AsOperation.Order.LET > new DateTime(2000, 1, 1) && operation.AsOperation.Order.LET < MinimumLET)
                {
                    MinimumLET = operation.AsOperation.Order.LET;
                }

                Operations.Add(operation.Code, operation);
                operation.Group = this;
                DispatchingOrder.Add(operation.Code, operation.DispatchingOrder);
            }
        }
    }
}

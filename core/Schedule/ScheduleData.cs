﻿using AsLib;
using System.Collections.Generic;
using System;

namespace graphIT.Asprova.Plugin.Schedule
{
    public class ScheduleData : IDisposable
    {
        private Asprova asprova;
        public Dictionary<string, Operation> operations = new Dictionary<string, Operation>();
        public Dictionary<int, Group> groups = new Dictionary<int, Group>();

        public ScheduleData(Asprova asprova)
        {
            this.asprova = asprova;

            ASOObjectList operationList = asprova.cmdParent.OperationList;
            CollectOperations(operationList);

            operationList.Clear();

            BuildGroups();
        }

        public void Dispose()
        {
            groups.Clear();
            operations.Clear();
        }

        private void CollectOperations(ASOObjectList operationList)
        {
            operationList.SortByExprStr("ME.API_DispatchingOrderDefault,a");

            // Store operations
            ASBOperationEx operation;
            for (int i = 1; i <= operationList.ObjectCount; i++)
            {
                operation = (ASBOperationEx)operationList.Object[i];
                var o = AddOperation(operation, this);
                o.DispatchingOrder = i;
            }

            // Collect previouse and nex operation chains for every operation
            foreach (Operation op in operations.Values)
            {
                int count = op.AsOperation.PrevOperationCount;
                for (int i = 1; i <= count; i++)
                {
                    if (operations.ContainsKey(op.AsOperation.PrevOperation[i].Code))
                    {
                        op.PrevOperations.Add(operations[op.AsOperation.PrevOperation[i].Code]);
                    }
                }

                count = op.AsOperation.NextOperationCount;
                var count2 = 0;
                for (int i = 1; i <= count; i++)
                {
                    if (operations.ContainsKey(op.AsOperation.NextOperation[i].Code))
                    {
                        op.NextOperations.Add(operations[op.AsOperation.NextOperation[i].Code]);
                        count2++;
                    }
                }
                if (count2 == 0) op.IsLast = true;
            }
        }

        private Operation AddOperation(ASBOperationEx operation, ScheduleData data)
        {
            var code = operation.Code;

            if (!operations.ContainsKey(code))
            {
                operations.Add(code, new Operation(asprova, operations, operation, data));
            }

            return operations[code];
        }

        #region "Group building"
        private void BuildGroups()
        {
            List<string> opProcessed = new List<string>();

            // Create groups
            int groupNo = 0;
            foreach (Operation operation in operations.Values)
            {
                if (operation.Group != null) continue;

                // Create new group, Increase next group id
                groupNo++;

                // Register to group
                if (!groups.ContainsKey(groupNo)) groups.Add(groupNo, new Group(groups, groupNo));
                groups[groupNo].AddOperation(operation);

                opProcessed.Add(operation.Code);

                DiscoverOperationPrevNext(groupNo, operation, ref opProcessed);
            }
        }

        private void DiscoverOperationPrevNext(int groupNo, Operation operation, ref List<string> opProcessed)
        {
            foreach (Operation childOperation in operation.PrevOperations)
            {
                if (opProcessed.Contains(childOperation.Code)) continue;
                if (!operations.ContainsKey(childOperation.Code)) continue;

                opProcessed.Add(childOperation.Code);

                groups[groupNo].AddOperation(childOperation);

                DiscoverOperationPrevNext(groupNo, childOperation, ref opProcessed);
            }

            foreach (Operation childOperation in operation.NextOperations)
            {
                if (opProcessed.Contains(childOperation.Code)) continue;
                if (!operations.ContainsKey(childOperation.Code)) continue;

                opProcessed.Add(childOperation.Code);

                groups[groupNo].AddOperation(childOperation);

                DiscoverOperationPrevNext(groupNo, childOperation, ref opProcessed);
            }
        }
        #endregion
    }
}

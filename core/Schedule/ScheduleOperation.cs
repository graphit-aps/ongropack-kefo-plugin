﻿using AsLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using static graphIT.Asprova.Plugin.AsCont;

namespace graphIT.Asprova.Plugin.Schedule
{
    [DebuggerDisplay("{Code}")]
    public class Operation
    {
        public Dictionary<string, Operation> parent;

        public List<Operation> PrevOperations { get; set; } = new List<Operation>();
        public List<Operation> NextOperations { get; set; } = new List<Operation>();

        public string Code { get; set; }
        public string Process { get; set; }
        public ASBOperationEx AsOperation { get; set; }
        public Group Group { get; set; }

        public DateTime InfiniteEST { get; set; } = DateTime.MinValue;
        public DateTime InfinitelLET { get; set; } = DateTime.MaxValue;

        public Direction OriginalDirection { get; set; }

        public int ForwardAssignmentOrder { get; set; } = 0;
        public int DefaultAssignmentOrder { get; set; } = 0;

        public int DispatchingOrder { get; set; } = 0;

        public bool IsLast { get; set; } = false;

        public Operation(AsprovaSchedule app, Dictionary<string, Operation> parent, ASBOperationEx asOperation, ScheduleData data)
        {
            Code = asOperation.Code;
            Process = asOperation.Master == null ? null : asOperation.Master.Code;
            this.parent = parent;
            AsOperation = asOperation;

            var prop = app.getPropertyDef("WorkUser_TheoreticalEST");
            if (prop != null)
            {
                var value = AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0);
                if (value != "") InfiniteEST = DateTime.Parse(value, CultureInfo.InvariantCulture);
            }

            prop = app.getPropertyDef("WorkUser_TheoreticalLET");
            if (prop != null)
            {
                var value = AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0);
                if (value != "") InfinitelLET = DateTime.Parse(value, CultureInfo.InvariantCulture);
            }

            prop = app.getPropertyDef("WorkUser_API_DispatchingOrderForward");
            if (prop != null)
            {
                var value = AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0);
                if (value != "") ForwardAssignmentOrder = int.Parse(value);
            }
            prop = app.getPropertyDef("WorkUser_API_DispatchingOrderDefault");
            if (prop != null)
            {
                var value = AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0);
                if (value != "") DefaultAssignmentOrder = int.Parse(value);
            }

            prop = app.getPropertyDef("WorkUser_API_AssignmentDirection");
            if (prop != null)
            {
                var value = AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0);
                if (value == "B" || value == "FB") OriginalDirection = Direction.Backward;
                else if (value == "F" || value == "FF") OriginalDirection = Direction.Forward;
                else throw new Exception("Operation without API assignment direction: " + Code);
            }
        }
    }
}

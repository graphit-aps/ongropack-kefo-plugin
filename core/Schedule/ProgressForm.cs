﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace graphIT.Asprova.Plugin.Schedule
{
    public partial class ProgressForm : Form
    {
        private ScheduleLogic sl = null;

        public System.Timers.Timer tim = new System.Timers.Timer();

        public ProgressForm(ScheduleLogic sl)
        {
            this.sl = sl;

            InitializeComponent();
        }

        private void Tim_Elapsed1(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (sl == null || sl.Abort || sl.OperationProcessed >= sl.OperationNumber)
            {
                sl = null;

                //DialogResult = DialogResult.OK;
                this.Invoke((MethodInvoker)delegate
                {
                    // close the form on the forms thread
                    this.Close();
                });

                return;
            }

            Action act;

            act = () => processedOperationNr.Text = sl.OperationProcessed.ToString();
            processedOperationNr.Invoke(act);

            int oPct = sl.OperationProcessed * 100 / sl.OperationNumber;
            act = () => operationProgress.Value = oPct;
            operationProgress.Invoke(act);

            act = () => operationPercent.Text = oPct.ToString();
            operationPercent.Invoke(act);

            tim.Enabled = true;
        }

        private void ProgressForm_Load(object sender, EventArgs e)
        {
            operationNr.Text = sl.OperationNumber.ToString();


            tim.Interval = 100;
            tim.Elapsed += Tim_Elapsed1;
            tim.AutoReset = false;
            tim.Start();
        }
    }
}

﻿using AsLib;
using AsPlugInManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using static graphIT.Asprova.Plugin.AsCont;

namespace graphIT.Asprova.Plugin.Schedule
{
    public class ScheduleLogic : IDisposable
    {
        public int OperationNumber { get; set; }
        public int OperationProcessed { get; set; }

        private Asprova asprova = null;
        private Thread th = null;

        private ScheduleData data = null;

        public bool Abort { get; set; } = false;


        public ScheduleLogic(Asprova asprova, ScheduleData data)
        {
            this.asprova = asprova;
            this.data = data;

            OperationNumber = data.operations.Count;

            OperationProcessed = 0;
            th = new Thread(new ThreadStart(ShowProgressForm));
            th.Start();

            SortedDictionary<int, Group> orderedGroups = new SortedDictionary<int, Group>();

            foreach (Group g in data.groups.Values)
                orderedGroups.Add(g.MinDispatchingOrder, g);

            foreach (Group g in orderedGroups.Values)
            {
                if (!SchedulingAssignGroup(g))
                    return;
            }

            Abort = true;
        }

        public void Dispose()
        {
            Abort = true;
            data = null;

            if (th != null && th.IsAlive)
            {
                th.Abort();
                th = null;
            }
                
        }

        private void ShowProgressForm()
        {
            var pf = new ProgressForm(this);
            pf.ShowDialog();

            pf = null;
        }

        private bool SchedulingAssignGroup(Group group)
        {
            // Stores all operations in the group for forward scheduling and correction
            SortedDictionary<int, Operation> fwAssignList = new SortedDictionary<int, Operation>();
            // Stores all operations in the group for default scheduling
            SortedDictionary<int, Operation> defAssignList = new SortedDictionary<int, Operation>();

            foreach (Operation o in group.Operations.Values)
            {
                fwAssignList.Add(o.ForwardAssignmentOrder, o);
                defAssignList.Add(o.DefaultAssignmentOrder, o);
            }

            bool hasBw = false;
            string errorCode = "";

            // SCHEDULING 1: Default schedulig
            bool success = ScheduleByList(defAssignList, ref hasBw, ref errorCode);

            if (success)
                return true;

            if(!hasBw)
                throw new Exception("Error in operation assignment: " + errorCode);


            // SCHEDULING 2 (FORWARD CORRECTION) forward scheduling in case of backward error

            // Unassign all assigned operation
            foreach (Operation o in defAssignList.Values)
            {

                if (o.AsOperation.IsAssigned == TIsAssigned.kIsAssignedUnassigned)
                    break;

                o.AsOperation.Unassign();

                OperationProcessed--;
            }

            success = ScheduleByList(fwAssignList, ref hasBw, ref errorCode, Direction.Forward);

            

            if (!success)
                throw new Exception("Error in operation assignment: " + errorCode);

            foreach (Operation o in fwAssignList.Values)
            {
                if (!o.IsLast)
                {
                    o.AsOperation.Unassign();

                    OperationProcessed--;
                }
            }



            // SCHEDULING 3 (Default scheduling with fixed last operations)
            success = ScheduleByList(defAssignList, ref hasBw, ref errorCode);

            if (!success)
            {
                var reassignedOperations = new List<string>();

                foreach (Operation o in fwAssignList.Values)
                {
                    if (o.AsOperation.IsAssigned == TIsAssigned.kIsAssignedUnassigned || o.AsOperation.StartTime < o.AsOperation.TotalCalculatedEST)
                    {
                        reassignedOperations.Add(o.Code);

                        if(o.AsOperation.IsAssigned != TIsAssigned.kIsAssignedUnassigned)
                        {
                            o.AsOperation.Unassign();
                            OperationProcessed--;
                        }
                        

                        success = asprova.Assign(o.AsOperation, Direction.Forward);

                        OperationProcessed++;

                        if (!success)
                            throw new Exception("Error in operation assignment: " + errorCode);
                    }
                }

                foreach (Operation o in defAssignList.Values)
                {
                    if (true || reassignedOperations.Contains(o.Code))
                    {
                        var starttime = o.AsOperation.StartTime;

                        if (o.AsOperation.IsAssigned != TIsAssigned.kIsAssignedUnassigned)
                        {
                            o.AsOperation.Unassign();
                            OperationProcessed--;
                        }

                        success = asprova.Assign(o.AsOperation, o.OriginalDirection);

                        OperationProcessed++;

                        if (!success || o.AsOperation.StartTime < o.AsOperation.TotalCalculatedEST)
                        {
                            success = asprova.Assign(o.AsOperation, Direction.Forward);

                            if (!success)
                                throw new Exception("Error in operation assignment: " + errorCode);
                        }
                    }
                }
            }

            return success;
        }

        private bool ScheduleByList(SortedDictionary<int, Operation> AssignList, ref bool hasBw, ref string errorCode, Direction dir = null)
        {
            bool success = true;
            foreach (Operation o in AssignList.Values)
            { 
                if (o.AsOperation.IsAssigned != TIsAssigned.kIsAssignedUnassigned) continue;

                if (dir == null)
                    dir = o.OriginalDirection;

                if (o.OriginalDirection == Direction.Backward) hasBw = true;

                success = asprova.Assign(o.AsOperation, dir);

                OperationProcessed++;

                if (!success)
                {
                    if (o.AsOperation.StartTime > asprova.proj.SchedulingStartTimeAsDATE)
                        throw new Exception("Operation assignment error! Affected operation: " + o.AsOperation.Code);
                    else
                    {
                        errorCode = o.Code;
                        return false;
                    }
                }
                else
                {
                    if (o.AsOperation.StartTime < o.AsOperation.TotalCalculatedEST)
                    {
                        errorCode = o.Code;
                        return false;
                    }
                }
            }

            return success;
        }
    }
}

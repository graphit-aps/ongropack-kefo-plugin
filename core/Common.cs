﻿using System;
using System.IO;
using System.Management;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Runtime.InteropServices;

namespace graphIT.Asprova.Plugin
{
    public static class Common
    {
        public static Config GetConfigCommon()
        {
            return GetConfig<Config>("common.json");
        }

        public static T GetConfig<T>(string configName)
        {
            var cfgLines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/configs", configName), UTF8Encoding.UTF8);
            return JsonConvert.DeserializeObject<T>(String.Join("\r\n", cfgLines));
        }

        [DllImport("mpr.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern int WNetGetConnection([MarshalAs(UnmanagedType.LPTStr)] string localName, [MarshalAs(UnmanagedType.LPTStr)] StringBuilder remoteName, ref int length);

        public static string GetUNCPath(string originalPath)
        {
            StringBuilder sb = new StringBuilder(512);
            int size = sb.Capacity;
            // look for the {LETTER}: combination ...

            if (originalPath.Length > 2 && originalPath[1] == ':')
            {

                // don't use char.IsLetter here - as that can be misleading

                // the only valid drive letters are a-z && A-Z.

                char c = originalPath[0];
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))

                {
                    int error = WNetGetConnection(originalPath.Substring(0, 2), sb, ref size);

                    if (error == 0)
                    {
                        DirectoryInfo dir = new DirectoryInfo(originalPath);
                        string path = Path.GetFullPath(originalPath).Substring(Path.GetPathRoot(originalPath).Length);
                        return Path.Combine(sb.ToString().TrimEnd(), path);
                    }
                }
            }

            return originalPath;

        }
    }
}

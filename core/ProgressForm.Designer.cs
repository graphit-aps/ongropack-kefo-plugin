﻿namespace AspBosal.core
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.totalOpNrLabel = new System.Windows.Forms.Label();
            this.processedOpNrLabel = new System.Windows.Forms.Label();
            this.operationProgress = new System.Windows.Forms.ProgressBar();
            this.processedOperationNr = new System.Windows.Forms.Label();
            this.operationNr = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.operationPercent = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // totalOpNrLabel
            // 
            this.totalOpNrLabel.AutoSize = true;
            this.totalOpNrLabel.Location = new System.Drawing.Point(44, 9);
            this.totalOpNrLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.totalOpNrLabel.Name = "totalOpNrLabel";
            this.totalOpNrLabel.Size = new System.Drawing.Size(84, 13);
            this.totalOpNrLabel.TabIndex = 3;
            this.totalOpNrLabel.Text = "Összes művelet:";
            // 
            // processedOpNrLabel
            // 
            this.processedOpNrLabel.AutoSize = true;
            this.processedOpNrLabel.Location = new System.Drawing.Point(11, 32);
            this.processedOpNrLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.processedOpNrLabel.Name = "processedOpNrLabel";
            this.processedOpNrLabel.Size = new System.Drawing.Size(119, 13);
            this.processedOpNrLabel.TabIndex = 4;
            this.processedOpNrLabel.Text = "Feldolgozott műveletek:";
            // 
            // operationProgress
            // 
            this.operationProgress.Location = new System.Drawing.Point(11, 56);
            this.operationProgress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.operationProgress.Name = "operationProgress";
            this.operationProgress.Size = new System.Drawing.Size(158, 19);
            this.operationProgress.TabIndex = 5;
            // 
            // processedOperationNr
            // 
            this.processedOperationNr.AutoSize = true;
            this.processedOperationNr.Location = new System.Drawing.Point(132, 32);
            this.processedOperationNr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.processedOperationNr.Name = "processedOperationNr";
            this.processedOperationNr.Size = new System.Drawing.Size(13, 13);
            this.processedOperationNr.TabIndex = 8;
            this.processedOperationNr.Text = "0";
            // 
            // operationNr
            // 
            this.operationNr.AutoSize = true;
            this.operationNr.Location = new System.Drawing.Point(132, 9);
            this.operationNr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.operationNr.Name = "operationNr";
            this.operationNr.Size = new System.Drawing.Size(13, 13);
            this.operationNr.TabIndex = 9;
            this.operationNr.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(192, 61);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "%";
            // 
            // operationPercent
            // 
            this.operationPercent.AutoSize = true;
            this.operationPercent.Location = new System.Drawing.Point(174, 61);
            this.operationPercent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.operationPercent.Name = "operationPercent";
            this.operationPercent.Size = new System.Drawing.Size(13, 13);
            this.operationPercent.TabIndex = 13;
            this.operationPercent.Text = "0";
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(230, 86);
            this.ControlBox = false;
            this.Controls.Add(this.operationPercent);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.operationNr);
            this.Controls.Add(this.processedOperationNr);
            this.Controls.Add(this.operationProgress);
            this.Controls.Add(this.processedOpNrLabel);
            this.Controls.Add(this.totalOpNrLabel);
            this.Location = new System.Drawing.Point(0, 40);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ProgressForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Folyamatban...";
            this.Load += new System.EventHandler(this.ProgressForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label totalOpNrLabel;
        private System.Windows.Forms.Label processedOpNrLabel;
        private System.Windows.Forms.ProgressBar operationProgress;
        private System.Windows.Forms.Label processedOperationNr;
        private System.Windows.Forms.Label operationNr;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label operationPercent;
    }
}
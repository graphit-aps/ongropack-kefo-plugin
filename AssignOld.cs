﻿using AsLib;
using AsPlugInManager;
using graphIT.Asprova.Plugin;
using System;
using System.Collections.Generic;
using System.Reflection;
using static AsPlugInManager.TReturnType;
using static graphIT.Asprova.Plugin.AssignHelper;

namespace AspBosal
{
    public class AssignOld
    {
        public static List<ParameterOld> parameters = new List<ParameterOld>();
        public static Dictionary<string, AssignHelper> helpers = new Dictionary<string, AssignHelper>();

        public static Asprova asprova = null;

        #region "Asprova"
        /// <summary>
        /// Called manually with a custom command
        /// </summary>
        /// <param name="argList"></param>
        /// <returns></returns>
        public TReturnType RescheduleStart(IASPArgList argList)
        {
            Init(argList);

            return kContinue;
        }

        /// <summary>
        /// Called manually with a custom command
        /// </summary>
        /// <param name="argList"></param>
        /// <returns></returns>
        public TReturnType RescheduleEnd(IASPArgList argList)
        {
            if (CurrentHelper() != null)
            {
                if (ParameterFlagExists("SaveForwardOrder"))
                {
                    CurrentHelper().RecordOrder((ASPArgList)argList, "Forward");
                }

                if (ParameterFlagExists("SaveFinalOrder"))
                {
                    CurrentHelper().RecordOrder((ASPArgList)argList, "Final");
                }
            }

            DestroyCurrentParameter();

            if (parameters.Count == 0 && asprova != null)
            {
                asprova.Dispose();
                asprova = null;
            }

            return kContinue;
        }

        public TReturnType BeforeAssign(IASPArgList argList)
        {
            if (!IsCurrentHelperValid() || ParameterFlagExists("NoEvent")) return kContinue;

            CurrentHelper().BeforeAssign((ASPArgList)argList, GetCurrentParameter());

            return kContinue;
        }

        public TReturnType AfterAssign(IASPArgList argList)
        {
            if (!IsCurrentHelperValid() || ParameterFlagExists("NoEvent")) return kContinue;

            CurrentHelper().AfterAssign((ASPArgList)argList, GetCurrentParameter());

            return kContinue;
        }

        public TReturnType AfterReschedule(IASPArgList argList)
        {
            parameters.Clear();
            foreach(AssignHelper h in helpers.Values)
            {
                h.Dispose();
            }

            return kContinue;
        }
        #endregion

        #region "Helper"
        private void Init(IASPArgList argList)
        {
            // Create Asprova object
            if (asprova != null) asprova.Dispose();
            asprova = new Asprova((ASPArgList)argList);

            // Load helper
            parameters.Add(null);

            var asprop = asprova.getPropertyDef("DefaultSchedulingParameterUser_API_AssignParameter");
            if (asprop != null)
            {
                var param = asprova.parentCommands[0].GetAsStr((TPropertyID)asprop.PropertyID, 1);
                param = param == "" ? null : param;

                if (param != null)
                {
                    var p = new ParameterOld(param);

                    if (helpers.ContainsKey(p.Helper) || CreateAssignHelper(p))
                    {
                        parameters[parameters.Count - 1] = p;
                    }
                }
            }
        }

        private bool CreateAssignHelper(ParameterOld parameter)
        {
            Assembly mscorlib = Assembly.GetExecutingAssembly();
            foreach (Type type in mscorlib.GetTypes())
            {
                if (type.BaseType != null && type.BaseType == typeof(AssignHelper))
                {
                    ParameterAttribute attr = (ParameterAttribute)type.GetCustomAttribute(typeof(ParameterAttribute));
                    if (attr.Name != parameter.Helper) continue;

                    helpers[parameter.Helper] = (AssignHelper)Activator.CreateInstance(type, new object[] { asprova });
                    return true;
                }
            }

            return false;
        }

        private bool IsCurrentHelperValid()
        {
            var parameter = GetCurrentParameter();
            if (parameter == null) return false;
            if (parameter.Helper == null) return false;

            return helpers.ContainsKey(parameter.Helper);
        }

        private AssignHelper CurrentHelper()
        {
            if (IsCurrentHelperValid() == false) return null;

            return helpers[GetCurrentParameter().Helper];
        }

        private bool ParameterFlagExists(string flag, ParameterOld parameter = null)
        {
            parameter = parameter ?? GetCurrentParameter();

            if (parameter == null) return false;

            return parameter.Flags.Contains(flag);
        }

        private ParameterOld GetCurrentParameter()
        {
            if (parameters.Count == 0) return null;

           return parameters[parameters.Count - 1];
        }

        private void DestroyCurrentParameter()
        {
            if (parameters.Count == 0) return;

            if (!ParameterFlagExists("KeepHelper"))
            {
                if (CurrentHelper() != null)
                {
                    CurrentHelper().Dispose();
                }
            }

            parameters.RemoveAt(parameters.Count - 1);
        }
        #endregion
    }
}

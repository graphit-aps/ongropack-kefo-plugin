﻿using System;

namespace AspOngropackKEFO
{
    using System.Runtime.InteropServices;
    using AsPlugInManager;

    [Guid("73D486BC-7232-403F-B3A9-325AED7F66F2"),
    InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IASDefault
    {
        [DispId(100)]
        bool AutoRegistration([In, MarshalAs(UnmanagedType.Interface)]IASPPlugInManager plugInManager, [In, MarshalAs(UnmanagedType.Interface)]IASPModule mod);
    }

    [Guid("4862EFAD-95A2-4D9A-80F5-A5F014E87CCE"),
    ClassInterface(ClassInterfaceType.None)]
    public class ASDefault : IASDefault
    {
        public bool AutoRegistration(IASPPlugInManager plugInManager, IASPModule mod)
        {
            string asmName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            ASPPlugIn plugIn;
            string progID = "";

            // Comment for module
            mod.Comment = "Asprova Ongropack Plugin by graphIT";

            /* Data IO */
            progID = asmName + ".CalculateComerio";
            plugIn = plugInManager.AddASPlugIn("[Ongropack] CalculateComerio", progID, "ComerioCalc", ASPlugInKeyName.KeyHookGeneric);
           // plugIn.Order = 2;

            plugIn = null;

            // Garbage collector
            GC.Collect();

            return true;
        }


        // Same as DLLRegister for c++
        [ComRegisterFunction]
        public static void DLLRegisterEquivalent(string reg)
        {
            //Console.WriteLine("Invoked By System when Registration is called");
        }

        // Same as DLLUnregister for c++
        [ComUnregisterFunction]
        public static void DLLUnregisterEquivalent(string reg)
        {
            //Console.WriteLine("Invoked By System whem Unregistration is called");
        }

    }
}

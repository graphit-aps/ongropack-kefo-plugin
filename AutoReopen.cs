﻿using AsLib;
using AsPlugInManager;
using graphIT.Asprova.Plugin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using static AsPlugInManager.TReturnType;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Timers;
using System.Threading;
using System.Data;

namespace AspOngropackKEFO
{
    public class AutoReopen
    {

        public TReturnType CheckReopenNeed2(IASPArgList argList)
        {
            return graphIT.Asprova.Plugin.AutoReopen.CheckReopenNeed2(argList);

        }

        public TReturnType SerializeCheck2(IASPArgList argList)
        {
            return graphIT.Asprova.Plugin.AutoReopen.SerializeCheck2(argList);
        }

        public TReturnType CheckNewVersion2(IASPArgList argList)
        {
            return graphIT.Asprova.Plugin.AutoReopen.CheckNewVersion2(argList);

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace AspOngropackKEFO
{
    public static class Utils
    {
        public static void Release(object obj)
        {
            if (obj != null)
            {
                Marshal.FinalReleaseComObject(obj);
                obj = null;
            }
        }

        public static string LoadSQLStatement(string statementName, Dictionary<string, string> parameters = null)
        {
            string sqlStatement = string.Empty;

            var ns = new StackTrace().GetFrame(1).GetMethod().ReflectedType.Namespace; ;

            using (Stream stm = Assembly.GetCallingAssembly().GetManifestResourceStream($"{ns}.{statementName.Replace('/', '.')}"))
            {
                if (stm != null)
                    sqlStatement = new StreamReader(stm).ReadToEnd();
                else
                    throw new Exception($"Lekérdezés nem található {statementName}!");
            }

            if (parameters != null)
            {
                foreach (var kvp in parameters)
                {
                    sqlStatement = sqlStatement.Replace(kvp.Key, kvp.Value);
                }
            }

            return sqlStatement;
        }

        public static TR ExtSafe<T, TR>(this T obj, Func<T, TR> func, Func<TR> ifNull = null)
        {
            return obj != null ? func(obj) : (ifNull != null ? ifNull() : default(TR));
        }

        public static TR Ext<T, TR>(this T obj, Func<T, TR> func)
        {
            return func(obj);
        }
    }
}

﻿using AsPlugInManager;
using System;
using static AsPlugInManager.TReturnType;

namespace AspOngropackKEFO
{
    public class Schedule
    {
        public TReturnType Scheduling(ASPArgList argList)
        {
            var schedule =  new graphIT.Asprova.Plugin.Schedule.Schedule();

            return schedule.Scheduling(argList);
        }
    }
}

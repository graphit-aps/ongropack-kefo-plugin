﻿using AsLib;
using AsPlugInManager;
using graphIT.Asprova.Plugin;
using AspOngropackKEFO.Class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using static AsPlugInManager.TReturnType;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Timers;
using System.Threading;
using System.Data;
using System.Linq;


namespace AspOngropackKEFO
{
    public class CalculateComerio
    {        
        // E három esetében az "eredeti minta" szerint public volt, de azzal probléma volt: Inconsistent Accesibility (A ComerioOperation az új)
        private Dictionary<string, Resource> Resources = new Dictionary<string, Resource>();
        private Dictionary<string, Operation> Operations = new Dictionary<string, Operation>();
        private List<Operation> ComerioOperations = new List<Operation>();
        private List<Order> ComerioOperationOrders = new List<Order>();
        private Dictionary<string, Order> Orders = new Dictionary<string, Order>();
        private Dictionary<string, Item> Items = new Dictionary<string, Item>();

        private ASBProjectEx myproject = null;

        public TReturnType ComerioCalc(IASPArgList argList)
        {

            #region Used Strings
            string comerio = "Comerio";
            string jumboRollCalc = "Anyatekercs számlálás";
            #endregion

            try
            {
                graphIT.Asprova.Plugin.Asprova asp = new Asprova((ASPArgList)argList);

                myproject = (ASBProjectEx)argList.ArgAsObject[TArgTypeAsObject.kArgProject];        // A myproject, amiből az összes megkapott adat szármízik
                var myroot = (ASORootObject)argList.ArgAsObject[TArgTypeAsObject.kArgRootObject];   // A root a project szülője
                ASFUtility myutil = myroot.Utility;

                Dictionary<string, TPropertyID> userDefProperties = new Dictionary<string, TPropertyID>();
                userDefProperties = GetUserProperties(myroot);

                // Test: tételek föltöltése, mert az operationön belül probléma volt
                for (int i = 1; i <= myproject.RootItem.ChildCount; i++)
                {
                    var asItem = myproject.RootItem.ChildAsItem[i] as ASBItemEx;
                    AddItem(asItem, false, myproject, userDefProperties);
                }

                //Az orderekhez hozzárendeljük a nextordereket
                for (int i = 1; i <= myproject.RootOrder.ChildCount; i++)
                {
                    var actualOrder = myproject.RootOrder.ChildAsOrder[i] as ASBOrder;
                    if (Orders.ContainsKey(actualOrder.Code) && !actualOrder.Disabled)
                    {
                        for (int j = 1; j <= actualOrder.RightmostOrderList.ObjectCount; j++)
                        {
                            Order actualNextOrder = Orders[actualOrder.RightmostOrderList.Object[j].Code];
                            Orders[actualOrder.Code].NextOrders.Add(actualNextOrder);
                        }
                    }
                }

                CollectOperations(myproject, userDefProperties, comerio);     // Összegyűjtjük az összes műveletet, illetve külön, amelyik Comerio

                CalculateRolls(userDefProperties);

                return kContinue;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return kError;
            }
            #region tryFinally
            finally
            {
                myproject.BroadcastChanges();
                Operations.Clear();
                Resources.Clear();
                Orders.Clear();
                ComerioOperations.Clear();
                Items.Clear();
                var msgRoot = myproject.RootMessage.AddMsg(TMessageType.kGUIInfo, jumboRollCalc);

                GC.Collect();
            }
            #endregion
        }

        private void CollectOperations(ASBProjectEx asProj, Dictionary<string, TPropertyID> userProperties, string processtype = null)
        {
            var opList = asProj.GetOperationList(false);    // A project műveletei listába

            foreach (var asOp in opList._ForEach<ASBOperationEx>())     // Műveletek gyűjtése: összes és Comerio
            {
                if (asOp.Order.IsAllOperationsAssigned != TIsAssigned.kIsAssignedUnassigned)    // Csak akkor vizsgáljuk a műveletet, ha assigned
                {
                    var myOperation = AddOperation(asOp, Operations, userProperties);

                    if (processtype == myOperation.MainResource.Code && myOperation.OperationOrder.AssignmentFlag)    // Ha Comerio, akkor ebbe a listába is bekerül
                    {
                        ComerioOperations.Add(myOperation);
                        ComerioOperationOrders.Add(myOperation.OperationOrder);
                    }
                }
            }
        }

        private Operation AddOperation(ASBOperationEx asOp, Dictionary<string, Operation> targetOperations, Dictionary<string, TPropertyID> userProperties) // Művelet hozzáadása a megfelelő műveleti listához
        {
            var op = new Operation(asOp);    // Új művelet objektum

            op.MainResource = AddResource(asOp.OperationMainRes, Resource.TType.Machine);   //Művelet erőforrásának megadása, és erőforrás tábla bővítés
            op.MainItem = AddItem((ASBItemEx)asOp.OperationOutMainItem, false, myproject, userProperties);   // Művelet kimeneti tétele
            op.OperationOrder = AddOrder(asOp.Order, op.MainItem, userProperties);                          // Művelet rendelése

            if (!targetOperations.ContainsKey(op.Code))
                targetOperations.Add(op.Code, op);

            return op;
        }

        private Resource AddResource(ASBResource asRes, Resource.TType type)    // Rendelés lista generálás
        {
            if (!Resources.ContainsKey(asRes.Code))     // Rendelés a listába, ha még nincsen
            {
                var res = new Resource(asRes);
                Resources.Add(res.Code, res);
            }

            return Resources[asRes.Code];
        }

        private Order AddOrder(ASBOrder asOrd, Item ordItem, Dictionary<string, TPropertyID> userProperties)      // Rendelések leképzése c#be
        {
            #region Used Strings
            string orderUserCalcLength = "OrderUser_CalcHossz";
            string orderUserCalcweight = "OrderUser_Calcweight";
            string orderUserOrderedTotal = "OrderUser_OrderedTotalL";
            string error = "error";     
            #endregion

            if (!Orders.ContainsKey(asOrd.Code))
            {
                var ord = new Order((ASBOrderEx)asOrd, ordItem)   // C# order objektum generálás
                {
                    DisabledFlag = asOrd.Disabled,
                    maxCalcHossz = asOrd.GetAsDouble(userProperties[orderUserCalcLength], 1),
                    maxCalcWeight = asOrd.GetAsDouble(userProperties[orderUserCalcweight], 1),
                    orderQty = asOrd.Qty,
                    orderedTotalLength = asOrd.GetAsDouble(userProperties[orderUserOrderedTotal], 1),
                };

                if (asOrd.IsAllOperationsAssigned != TIsAssigned.kIsAssignedUnassigned)
                {
                    ord.AssignmentFlag = true;
                }
                else
                    ord.AssignmentFlag = false;

                // Upload the nextorder exact pegged quantities (from Peg table)
                ASBOutputInstruction oi = (ASBOutputInstruction)asOrd.OutputInstructionListRecursive.Object[1];
                if (oi != null && asOrd.OrderType == TOrderType.kOrderTypeManufacturingOrder)
                {
                    for (int i = 1; i <= oi.PegCount; i++)
                    {
                        string peggedRightOrder = oi.Peg[i].RightOperation.Code.ToString();
                        peggedRightOrder = peggedRightOrder.Substring(0, peggedRightOrder.Length - 3);
                        ord.PeggedQuantities.Add(peggedRightOrder, oi.Peg[i].Qty);
                    }
                }

                //Ha a maximum átmérő adott
                //A disabled rendelések esetén nem érdekes a vizsgálat

                if (!ord.DisabledFlag)
                {
                    if (asOrd.Spec[1] != null)
                    {
                        ord.maxDiameter = asOrd.Spec[1].Code.ToString();
                    }
                    //Ha a maximum tömeg adott
                    else if (asOrd.Spec[2] != null)
                    {
                        ord.maxWeight = asOrd.Spec[2].Code.ToString();
                    }
                    //Amúgy
                    else
                    {
                        ord.maxDiameter = error;  // Ha egyik sincs, akkor ezt írjuk -> Asprovában lehet rá szűrni
                    }
                    if (asOrd.Spec[5] != null)
                        ord.cseveDiam = Convert.ToDouble(asOrd.Spec[5].Code.ToString());
                    else
                        ord.cseveDiam = 100;    //Egyelőre 100 --> az ívek esete miatt ez nem egyértelmű
                }                              

                Orders.Add(ord.Code, ord);
            }

            return Orders[asOrd.Code];
        }

        private Item AddItem(ASBItemEx asItem, bool component, ASBProjectEx asProj, Dictionary<string, TPropertyID> userProperties)
        {
            #region Used Strings
            string itemUserFoliaType = "ItemUser_FoliaType";
            string itemUserFoliaLength = "ItemUser_IvLength";
            #endregion

            if (!Items.ContainsKey(asItem.Code))
            {
                var item = new Item()
                {
                    Code = asItem.Code,
                    Object = asItem,
                    IsComponent = component,                    
                    //CustomLLC = (int)asItem.ExplodeNow(),
                };

                item.FoliaThick = asItem.NumSpec[1];
                item.FoliaWidth = asItem.NumSpec[2];
                item.Foliadensity = asItem.NumSpec[3];
                item.Foliakiszereles = asItem.GetAsStr(userProperties[itemUserFoliaType], 1);    //Ez igazából Asprovában enum: 0-Tekercs, 1-Ív
                if (item.Foliakiszereles == "1")
                {
                    //item.FoliaLength = asItem.GetAsStr(userProperties[itemUserFoliaLength], 1);
                    if (userProperties.ContainsKey(itemUserFoliaLength)) {
                        item.FoliaLength = asItem.GetAsStr(userProperties[itemUserFoliaLength], 1);
                    }

                }                
                Items.Add(item.Code, item);

                var objectCount = item.Object.OrderList.ObjectCount;    // Az adott tételhez tartozó objektumok
                // Létrehozzuk már itt a rendeléseket, hogy később már az Orders listából válogatni lehessen
                for (int i = 1; i <= objectCount; i++)
                {
                    var asOrder = item.Object.OrderList.Object[i] as ASBOrderEx;
                    AddOrder(asOrder as ASBOrder, item, userProperties);
                }
            }

            return Items[asItem.Code];
        }

        private void CalculateRolls(Dictionary<string, TPropertyID> userProperties)
        {
            #region Used Strings
            string orderUserComerioSize = "OrderUser_ComerioSize";
            string orderUserComerioKg = "OrderUser_ComerioKg";
            string orderUserComerioLength = "OrderUser_ComerioHossz";
            string orderUserComerioNum = "OrderUser_ComerioDb";
            string extraJumboNum = "Extra Jumbo darab";
            string extraJumboInLength = "Extra Jumbo hanyszoros";
            string baseJumboWeight = "Alap Jumbo tomeg";
            string baseJumboinLength = "Alap Jumbo hanyszoros";
            string baseJumboLength = "Alap Jumbo hossz";
            string baseJumboNum = "Alap Jumbo darab";
            string OrderUserPlugInMaxLength = "OrderUser_PlugInMaxLength";
            #endregion

            foreach (Operation myComerioOp in ComerioOperations)
            {
                //Ki kell szűrni azokat a műveleteket, amelyek unassigned-ek
                if (myComerioOp.OperationOrder.AssignmentFlag)
                {
                    double maxWidth = 1650;
                    double minWidth = 950;
                    double lengthRatio = 0.2;
                    double maxWeight = 1050;
                    double maxDiam = 950;
                    int maxRep = 16;

                    //1. Anyatekercs szélsesség: Fólia vastagság: 0.6 alatt max 1500, 0.6 fölött max 1100 - 2022.07.18 változott a max méret 1650-re
                    if (myComerioOp.MainItem.FoliaThick > 0.6)
                        maxWidth = 1100;

                    List<Item> containingItems = new List<Item>();
                    //List<Item> containingItemsordered = new List<Item>();
                    if (myComerioOp.OperationOrder.NextOrders.Count < 1)    // A Comeriora késztermék van kiírva
                        containingItems.Add(myComerioOp.MainItem);
                    else
                    {
                        for (int i = 0; i < myComerioOp.OperationOrder.NextOrders.Count; i++)
                        {
                            containingItems.Add(myComerioOp.OperationOrder.NextOrders[i].Item);
                        }
                    }
                    //containingItemsordered = containingItems.OrderByDescending(i => i.foliaWidth).ToList();

                    Dictionary<string, double> JumboLength = new Dictionary<string, double>();

                    //2. Comerion szeljük-e: ha 500 <= szélesség <= 750, ha nincs nextorder, akkor Comerión szeljük
                    //3. Szélességen belül max 16-szoros lehet a végtermék szélesség
                    //4. Tekercs max tömeg: 1050kg és max átmérő: 950

                    if (containingItems.Count == 1)      //1 kimenő tétele van a műveletnek
                    {
                        double peggedQtyLength;
                        double peggedQty;

                        if (myComerioOp.OperationOrder.NextOrders.Count < 1)     //Nincs szeletelési művelet hozzárendelve
                        {
                            if (double.TryParse(myComerioOp.OperationOrder.maxDiameter, out double maxDiam2))
                            {
                                // Ebben az esetben módosul a max átmérő és tömeg: a vevő által megadott max méretekkel számolunk a Comerióra
                                if (maxDiam2 > 0)
                                {
                                    maxDiam = maxDiam2;
                                }
                            }
                            if (Convert.ToDouble(myComerioOp.OperationOrder.maxWeight) > 0)
                            {
                                maxWeight = Convert.ToDouble(myComerioOp.OperationOrder.maxWeight);
                            }

                            if (myComerioOp.MainItem.FoliaWidth <= 750)      // Ha a szélessége kisebb, mint 750, akkor Comerión kettőbe szeljük
                            {
                                int JumboContainsWidth = 2;

                                maxWeight = JumboContainsWidth * maxWeight;
                                myComerioOp.OperationOrder.ComerioSize = string.Format("{0}/{1}/*{2}", JumboContainsWidth * myComerioOp.MainItem.FoliaWidth, myComerioOp.MainItem.FoliaWidth, myComerioOp.MainItem.FoliaThick);
                                
                                double JumboWidth = 2 * myComerioOp.MainItem.FoliaWidth;
                                double orderderTotalHossz = myComerioOp.OperationOrder.orderedTotalLength;
                                double itemDensity = myComerioOp.MainItem.Foliadensity;
                                double smallRollLength = myComerioOp.OperationOrder.maxCalcHossz;
                                double cseveDiam = myComerioOp.OperationOrder.cseveDiam;
                                double itemThickness = myComerioOp.MainItem.FoliaThick;

                                if (myComerioOp.OperationOrder.PeggedQuantities.Count > 0)
                                {
                                    peggedQty = myComerioOp.OperationOrder.PeggedQuantities[myComerioOp.OperationOrder.NextOrders[0].Code.ToString()];
                                    orderderTotalHossz = peggedQty / (myComerioOp.OperationOrder.NextOrders[0].orderQty) * orderderTotalHossz;
                                }

                                JumboLength = ComerioLengthScale(ref JumboWidth, orderderTotalHossz, itemDensity, ref smallRollLength, 
                                    ref JumboContainsWidth, cseveDiam, itemThickness, maxDiam, maxWeight, minWidth, lengthRatio, false);

                                myComerioOp.OperationOrder.FinalUsedMaxSmallRollLength = smallRollLength.ToString();
                            }
                            else                //Nincs szeletelés, Comerión leszereljük, és még ketté sem vágjuk
                            {
                                int JumboContainsWidth = 1;

                                myComerioOp.OperationOrder.ComerioSize = string.Format("{0}*{1}", myComerioOp.MainItem.FoliaWidth, myComerioOp.MainItem.FoliaThick);

                                double JumboWidth = myComerioOp.MainItem.FoliaWidth;
                                double orderderTotalHossz = myComerioOp.OperationOrder.orderedTotalLength;
                                double itemDensity = myComerioOp.MainItem.Foliadensity;
                                double smallRollLength = myComerioOp.OperationOrder.maxCalcHossz;
                                double cseveDiam = myComerioOp.OperationOrder.cseveDiam;
                                double itemThickness = myComerioOp.MainItem.FoliaThick;

                                if (myComerioOp.OperationOrder.PeggedQuantities.Count > 0)
                                {
                                    peggedQty = myComerioOp.OperationOrder.PeggedQuantities[myComerioOp.OperationOrder.NextOrders[0].Code.ToString()];
                                    orderderTotalHossz = peggedQty / (myComerioOp.OperationOrder.NextOrders[0].orderQty) * orderderTotalHossz;
                                }                                

                                JumboLength = ComerioLengthScale(ref JumboWidth, orderderTotalHossz, itemDensity, ref smallRollLength,
                                    ref JumboContainsWidth, cseveDiam, itemThickness, maxDiam, maxWeight, minWidth, lengthRatio, false);

                                myComerioOp.OperationOrder.FinalUsedMaxSmallRollLength = smallRollLength.ToString();
                            }                            
                        }
                        else    // 1 kimeneti tétele van a comerió műveletnek, de van szeletelés
                        {
                            if (containingItems[0].FoliaWidth <= 750 && containingItems[0].FoliaWidth >= 500)   // Lenne szeletelés, de mégis a Comerión ketté vágjuk
                            {
                                int JumboContainsWidth = 2;
                                double jumboSizeSecondParameter = containingItems[0].FoliaWidth;
                                if (containingItems[0].Foliakiszereles == "1")
                                {
                                    jumboSizeSecondParameter = Convert.ToDouble(containingItems[0].FoliaLength);
                                }

                                myComerioOp.OperationOrder.ComerioSize = string.Format("{0}/{1}/*{2}", JumboContainsWidth * containingItems[0].FoliaWidth, jumboSizeSecondParameter, containingItems[0].FoliaThick);

                                double JumboWidth = JumboContainsWidth * containingItems[0].FoliaWidth;
                                double orderderTotalHossz = myComerioOp.OperationOrder.NextOrders[0].orderedTotalLength;
                                double itemDensity = containingItems[0].Foliadensity;
                                double smallRollLength = myComerioOp.OperationOrder.NextOrders[0].maxCalcHossz;
                                double cseveDiam = myComerioOp.OperationOrder.NextOrders[0].cseveDiam;
                                double itemThickness = containingItems[0].FoliaThick;
                                if (containingItems[0].Foliakiszereles == "1")
                                {
                                    maxWeight = 500;
                                }

                                peggedQty = myComerioOp.OperationOrder.PeggedQuantities[myComerioOp.OperationOrder.NextOrders[0].Code.ToString()];
                                peggedQtyLength = peggedQty / (myComerioOp.OperationOrder.NextOrders[0].orderQty) * orderderTotalHossz;

                                JumboLength = ComerioLengthScale(ref JumboWidth, peggedQtyLength, itemDensity, ref smallRollLength,
                                    ref JumboContainsWidth, cseveDiam, itemThickness, maxDiam, maxWeight, minWidth, lengthRatio, false);

                                myComerioOp.OperationOrder.NextOrders[0].FinalUsedMaxSmallRollLength = smallRollLength.ToString();
                            }
                            else  // 1 kimeneti tétele van a comerió műveletnek, és szeletelőre megy és <500
                            {
                                double jumboSizeSecondParameter = containingItems[0].FoliaWidth;
                                int JumboContainsWidth = ComerioWidthScale(containingItems[0].FoliaWidth, maxRep, maxWidth);

                                double JumboWidth = JumboContainsWidth * containingItems[0].FoliaWidth;
                                double orderderTotalHossz = myComerioOp.OperationOrder.NextOrders[0].orderedTotalLength;
                                double itemDensity = containingItems[0].Foliadensity;
                                double smallRollLength = myComerioOp.OperationOrder.NextOrders[0].maxCalcHossz;
                                double cseveDiam = myComerioOp.OperationOrder.NextOrders[0].cseveDiam;
                                double itemThickness = containingItems[0].FoliaThick;
                                if (containingItems[0].Foliakiszereles == "1")
                                {
                                    maxWeight = 500;
                                }

                                peggedQty = myComerioOp.OperationOrder.PeggedQuantities[myComerioOp.OperationOrder.NextOrders[0].Code.ToString()];
                                peggedQtyLength = peggedQty / (myComerioOp.OperationOrder.NextOrders[0].orderQty) * orderderTotalHossz;

                                JumboLength = ComerioLengthScale(ref JumboWidth, peggedQtyLength, itemDensity, ref smallRollLength, 
                                    ref JumboContainsWidth, cseveDiam, itemThickness, maxDiam, maxWeight, minWidth, lengthRatio);
                                jumboSizeSecondParameter = JumboWidth / JumboContainsWidth;
                                if (containingItems[0].Foliakiszereles == "1")
                                {
                                    jumboSizeSecondParameter = Convert.ToDouble(containingItems[0].FoliaLength);
                                }

                                myComerioOp.OperationOrder.ComerioSize = string.Format("{0}/{1}/*{2}", JumboWidth, jumboSizeSecondParameter, itemThickness);
                                myComerioOp.OperationOrder.NextOrders[0].FinalUsedMaxSmallRollLength = smallRollLength.ToString();
                            }                            
                        }
                        myComerioOp.OperationOrder.ComerioWeight = JumboLength[baseJumboWeight].ToString();
                        if (JumboLength[extraJumboNum] != 0)
                        {
                            myComerioOp.OperationOrder.ComerioHossz = string.Format("{0} x hossz \n{1}", JumboLength[baseJumboinLength].ToString(), JumboLength[baseJumboLength].ToString());
                            if (JumboLength[baseJumboNum] == 0)
                            {
                                myComerioOp.OperationOrder.ComerioDb = string.Format("1x{0}x hossz", JumboLength[extraJumboInLength].ToString());
                            }
                            else
                                myComerioOp.OperationOrder.ComerioDb = string.Format("{0}x{1}x hossz \n1x{2}x hossz", JumboLength[baseJumboNum].ToString(), JumboLength[baseJumboinLength].ToString(), JumboLength[extraJumboInLength].ToString());
                        }
                        else
                        {
                            myComerioOp.OperationOrder.ComerioHossz = string.Format("{0} x hossz \n{1}", JumboLength[baseJumboinLength].ToString(), JumboLength[baseJumboLength].ToString());
                            myComerioOp.OperationOrder.ComerioDb = JumboLength[baseJumboNum].ToString();

                        }                        
                    }
                    else // A Comeriónak többféle kimeneti tétele is van
                    {
                        double peggedQtyLength;
                        double peggedQty;

                        List<Order> orderedNextorders = new List<Order>();
                        orderedNextorders = myComerioOp.OperationOrder.NextOrders;
                        foreach (Order actualnextOrder in orderedNextorders)
                        {
                            double jumboSizeSecondParameter = actualnextOrder.Item.FoliaWidth;
                            if (actualnextOrder.Item.Foliakiszereles == "1")// Ív a termék
                            {
                                maxWeight = 500;
                                jumboSizeSecondParameter = Convert.ToDouble(actualnextOrder.Item.FoliaLength);
                            }

                            if (actualnextOrder.Item.FoliaWidth <= 750 && actualnextOrder.Item.FoliaWidth >= 500)   // Lenne szeletelés, de mégis a Comerión ketté vágjuk
                            {
                                int JumboContainsWidth = 2;

                                actualnextOrder.ComerioSize = string.Format("{0}/{1}/*{2}", JumboContainsWidth * actualnextOrder.Item.FoliaWidth, jumboSizeSecondParameter, actualnextOrder.Item.FoliaThick);

                                double JumboWidth = JumboContainsWidth * actualnextOrder.Item.FoliaWidth;
                                double orderderTotalHossz = actualnextOrder.orderedTotalLength;
                                double itemDensity = actualnextOrder.Item.Foliadensity;
                                double smallRollLength = actualnextOrder.maxCalcHossz;
                                double cseveDiam = actualnextOrder.cseveDiam;
                                double itemThickness = actualnextOrder.Item.FoliaThick;

                                peggedQty = myComerioOp.OperationOrder.PeggedQuantities[actualnextOrder.Code.ToString()];
                                peggedQtyLength = peggedQty / (actualnextOrder.orderQty) * orderderTotalHossz;

                                JumboLength = ComerioLengthScale(ref JumboWidth, peggedQtyLength, itemDensity, ref smallRollLength,
                                    ref JumboContainsWidth, cseveDiam, itemThickness, maxDiam, maxWeight, minWidth, lengthRatio, false);

                                if (myComerioOp.OperationOrder.ComerioSize != null)
                                {
                                    myComerioOp.OperationOrder.ComerioSize += "; ";
                                    myComerioOp.OperationOrder.ComerioSize += string.Format("{0}/{1}/*{2}", JumboWidth, jumboSizeSecondParameter, itemThickness);
                                }
                                else
                                    myComerioOp.OperationOrder.ComerioSize = string.Format("{0}/{1}/*{2}", JumboWidth, jumboSizeSecondParameter, itemThickness);

                                actualnextOrder.FinalUsedMaxSmallRollLength = smallRollLength.ToString();
                            }
                            else  // 1 kimeneti tétele van a comerió műveletnek, és szeletelőre megy
                            {
                                int JumboContainsWidth = ComerioWidthScale(actualnextOrder.Item.FoliaWidth, maxRep, maxWidth);

                                double JumboWidth = JumboContainsWidth * actualnextOrder.Item.FoliaWidth;
                                double orderderTotalHossz = actualnextOrder.orderedTotalLength;
                                double itemDensity = actualnextOrder.Item.Foliadensity;
                                double smallRollLength = actualnextOrder.maxCalcHossz;
                                double cseveDiam = actualnextOrder.cseveDiam;
                                double itemThickness = actualnextOrder.Item.FoliaThick;

                                peggedQty = myComerioOp.OperationOrder.PeggedQuantities[actualnextOrder.Code.ToString()];
                                peggedQtyLength = peggedQty / (actualnextOrder.orderQty) * orderderTotalHossz;

                                JumboLength = ComerioLengthScale(ref JumboWidth, peggedQtyLength, itemDensity, ref smallRollLength, 
                                    ref JumboContainsWidth, cseveDiam, itemThickness, maxDiam, maxWeight, minWidth, lengthRatio);

                                if (myComerioOp.OperationOrder.ComerioSize != null)
                                {
                                    myComerioOp.OperationOrder.ComerioSize += "; ";
                                    myComerioOp.OperationOrder.ComerioSize += string.Format("{0}/{1}/*{2}", JumboWidth, jumboSizeSecondParameter, itemThickness);
                                }
                                else
                                    myComerioOp.OperationOrder.ComerioSize = string.Format("{0}/{1}/*{2}", JumboWidth, jumboSizeSecondParameter, itemThickness);

                                actualnextOrder.FinalUsedMaxSmallRollLength = smallRollLength.ToString();
                            }

                            if (myComerioOp.OperationOrder.ComerioWeight == null)
                            {
                                myComerioOp.OperationOrder.ComerioWeight = JumboLength[baseJumboWeight].ToString();
                                if (JumboLength[extraJumboNum] != 0)
                                {
                                    myComerioOp.OperationOrder.ComerioHossz = string.Format("{0} x hossz \n{1}", JumboLength[baseJumboinLength].ToString(), JumboLength[baseJumboLength].ToString());
                                    myComerioOp.OperationOrder.ComerioDb = string.Format("{0}x{1}x hossz \n1x{2}x hossz", JumboLength[baseJumboNum].ToString(), JumboLength[baseJumboinLength].ToString(), JumboLength[extraJumboInLength].ToString());
                                }
                                else
                                {
                                    myComerioOp.OperationOrder.ComerioHossz = string.Format("{0} x hossz \n{1}", JumboLength[baseJumboinLength].ToString(), JumboLength[baseJumboLength].ToString());
                                    myComerioOp.OperationOrder.ComerioDb = JumboLength[baseJumboNum].ToString();

                                }
                            }
                            else
                            {
                                myComerioOp.OperationOrder.ComerioWeight += "; ";
                                myComerioOp.OperationOrder.ComerioWeight += JumboLength[baseJumboWeight].ToString();
                                if (JumboLength[extraJumboNum] != 0)
                                {
                                    myComerioOp.OperationOrder.ComerioHossz += "; ";
                                    myComerioOp.OperationOrder.ComerioDb += "; ";
                                    myComerioOp.OperationOrder.ComerioHossz += string.Format("{0} x hossz \n{1}", JumboLength[baseJumboinLength].ToString(), JumboLength[baseJumboLength].ToString());
                                    myComerioOp.OperationOrder.ComerioDb += string.Format("{0}x{1}x hossz \n1x{2}x hossz", JumboLength[baseJumboNum].ToString(), JumboLength[baseJumboinLength].ToString(), JumboLength[extraJumboInLength].ToString());
                                }
                                else
                                {
                                    myComerioOp.OperationOrder.ComerioHossz += "; ";
                                    myComerioOp.OperationOrder.ComerioDb += "; ";
                                    myComerioOp.OperationOrder.ComerioHossz += string.Format("{0} x hossz \n{1}", JumboLength[baseJumboinLength].ToString(), JumboLength[baseJumboLength].ToString());
                                    myComerioOp.OperationOrder.ComerioDb += JumboLength[baseJumboNum].ToString();

                                }
                            }                            
                        }
                    }
                }
            }

            // Asprova interfész objektebe beírni a változott paramétereket
            for (int i = 1; i <= myproject.RootOrder.ChildCount; i++)
            {
                // Comerio gyártási rendelések paraméterei
                if (ComerioOperationOrders.Find(o => o.Code == myproject.RootOrder.ChildAsOrder[i].Code) != null)
                {
                    Order actualComerioOrder = ComerioOperationOrders.Find(o => o.Code == myproject.RootOrder.ChildAsOrder[i].Code);
                    myproject.RootOrder.ChildAsOrder[i].SetAsStr(userProperties[orderUserComerioSize], 1, actualComerioOrder.ComerioSize);
                    myproject.RootOrder.ChildAsOrder[i].SetAsStr(userProperties[orderUserComerioKg], 1, actualComerioOrder.ComerioWeight);
                    myproject.RootOrder.ChildAsOrder[i].SetAsStr(userProperties[orderUserComerioLength], 1, actualComerioOrder.ComerioHossz);
                    myproject.RootOrder.ChildAsOrder[i].SetAsStr(userProperties[orderUserComerioNum], 1, actualComerioOrder.ComerioDb);
                    //myproject.RootOrder.ChildAsOrder[i].SetAsStr(userProperties[OrderUserPlugInMaxLength], 1, actualComerioOrder.FinalUsedMaxSmallRollLength);
                }
                // A vevői rendelések esetén a ténylegesen használt kistekercs méret kerül be
                foreach (KeyValuePair<string, Order> myActualOrder in Orders)
                {
                    if (myproject.RootOrder.ChildAsOrder[i].Code == myActualOrder.Value.Code && !myActualOrder.Value.DisabledFlag
                        && myActualOrder.Value.Code.Substring(0, 2) != "I-")
                    {
                        if (userProperties.ContainsKey(OrderUserPlugInMaxLength))
                        {
                            myproject.RootOrder.ChildAsOrder[i].SetAsStr(userProperties[OrderUserPlugInMaxLength], 1, myActualOrder.Value.FinalUsedMaxSmallRollLength);
                        }
                    }             
                }
            }


        }

        // A Jumbo tekercs hossz és tekercsszám meghatározása
        // Visszatér:
            // "Alap Jumbo hossz" - Jumbo hossz m-ben
            // "Alap Jumbo darab" - Alap Jumbo tekercs szám
            // "Alap Jumbo tömeg" - Alap Jumbo tekercs tömege
            // "Alap Jumbo hanyszoros" - Hányszoros hossz egy Jumbo tekercs
            // "Extra Jumbo hossz" - maradék Jumbo tekercs hossza
            // "Extra Jumbo hanyszoros" - Extra Jumbo hányszoros hossz
        private Dictionary<string, double> ComerioLengthScale(ref double JumboWidth, double TotalOrderedLength, double itemDensity,
            ref double inputSmallRollLength, ref int inputDarabszel, double csevediam, double itemThickness, double maxDiam, double maxWeight, 
            double minWidth, double lengthRatio, bool allowWidthRed = true)
            // allowIdthRed: Endegélyezett-e a Jumbo szélesség csökkentés
        {
           
                Dictionary<string, double> Jumboproperties = new Dictionary<string, double>();
                double smallRollLength;
                // Megeshet, hogy a max tekercs méretből nagyobb hossz adódik, mint a teljes rendelt folyóméter
                if (inputSmallRollLength > TotalOrderedLength)
                {
                    smallRollLength = TotalOrderedLength;
                }
                else
                {
                    smallRollLength = inputSmallRollLength;
                }
                int darabszel = inputDarabszel; // Szélességben hány darab az anyatekercsben
                int numofSmallRoll = 1; // Hisszúságban hány kistekercs az anyatekercsben
                double baseJumboLenght = numofSmallRoll * smallRollLength;
                double totalLengthInJumbo = baseJumboLenght * darabszel;
                int numOfJumboBase = 0;
                double baseJumboWeight = Math.Truncate(0.001 * itemDensity * baseJumboLenght * itemThickness * JumboWidth);
                double extraJumboLength = 0;
                double extraJumboWeight = 0;
                int numofJumboExtra = 0;
                bool iterateAgain = false;

                double extraJumboLengthRest = extraJumboLength; //
                double producedExtraJumboLength = 0;
                double totalLengthInProdExtra = 0;
                int numofExtraSmallRoll = 0;
           // if (inputSmallRollLength != 0 && TotalOrderedLength <double.MaxValue) //ha nulla az OrderUser_calcHossz, akkor nem hajtódik végre
          //  {

                // Amíg nem éri el a Jumbo tekercs a megadott határokat (max tömeg, max szélesség, totál rendelt mennyiség), addig a kistekercsszeres hosszt növeljük
                // iterateAgain: ha a darabszel változót csökkentettük (a Jumbo szélességből leveszünk), akkor újra iterálunk
                // Vizsgált változó: numofSmallRoll, vagy a darabszel
                while (numOfJumboBase < 1 || iterateAgain)
                {
                    numofSmallRoll = 1;
                    baseJumboLenght = numofSmallRoll * smallRollLength;
                    totalLengthInJumbo = baseJumboLenght * darabszel;
                    numOfJumboBase = 0;
                    baseJumboWeight = Math.Truncate(0.001 * itemDensity * baseJumboLenght * itemThickness * JumboWidth);
                    extraJumboLength = 0;

                    double jumboDiam = Math.Sqrt(((4000 * baseJumboLenght * itemThickness) / Math.PI) + Math.Pow(csevediam, 2));

                    // Jumbo max átmérő, max tömeg vizsgálat + hogy 1 tekercs nem haladja meg a rendelt mennyiséget

                    while (jumboDiam <= maxDiam && totalLengthInJumbo <= TotalOrderedLength && baseJumboWeight <= maxWeight)
                    {
                        numofSmallRoll += 1;

                        baseJumboLenght = numofSmallRoll * smallRollLength;
                        baseJumboWeight = Math.Truncate(0.001 * itemDensity * baseJumboLenght * itemThickness * JumboWidth);
                        totalLengthInJumbo = baseJumboLenght * darabszel;
                        jumboDiam = Math.Sqrt(((4000 * baseJumboLenght * itemThickness) / Math.PI) + Math.Pow(csevediam, 2));
                    }

                    if (numofSmallRoll != 1)
                        numofSmallRoll -= 1;

                    baseJumboLenght = numofSmallRoll * smallRollLength;
                    totalLengthInJumbo = baseJumboLenght * darabszel;
                    baseJumboWeight = Math.Truncate(0.001 * itemDensity * baseJumboLenght * itemThickness * JumboWidth);

                    
                    numOfJumboBase = Convert.ToInt32(Math.Ceiling(TotalOrderedLength / totalLengthInJumbo));
                    extraJumboLength = Math.Round((TotalOrderedLength) - (numOfJumboBase * totalLengthInJumbo));
                    extraJumboLengthRest = extraJumboLength;
                    if (extraJumboLength >= smallRollLength * darabszel)    // Darabszel mellett 1 kistekercs hossz kisebb-e, mint a fennmaradó gyártandó mennyiség
                    {
                        do
                        {
                            numofExtraSmallRoll += 1;
                            producedExtraJumboLength = numofExtraSmallRoll * smallRollLength;
                            totalLengthInProdExtra = producedExtraJumboLength * darabszel;
                            extraJumboLengthRest = extraJumboLength - totalLengthInProdExtra;

                        } while (extraJumboLengthRest > 0); // Addig növelem az extra kistekercs hosszt, amíg van maradék a teljes rendelési mennyiségből
                        if (extraJumboLengthRest < 0) //&& Math.Abs(extraJumboLengthRest) > TotalOrderedLength * 0.1) --> ez erdetileg volt: mert a +-10%-ban úgyis benne volna
                        {
                            numofExtraSmallRoll--;
                            producedExtraJumboLength = numofExtraSmallRoll * smallRollLength;
                            totalLengthInProdExtra = producedExtraJumboLength * darabszel;
                            extraJumboLengthRest += smallRollLength * darabszel;
                        }
                        extraJumboWeight = Math.Truncate(0.001 * itemDensity * producedExtraJumboLength * itemThickness * JumboWidth);
                        numofJumboExtra = 1;
                    }

                    // Engedve van a darabszel csökkentés (nem fixen 1, vagy 2) és ha darabszel szerint nem jön ki 1 egész Jumbo, és az extrahossz több, mint 10% akkor csökkentjük a darabszel-t
                    if ((numOfJumboBase < 1 || (extraJumboLengthRest > TotalOrderedLength * 0.1 && darabszel > 1)) && allowWidthRed)
                    {
                        // (JumboWidth / darabszel) -- ő a kistekercs szélessége
                        if (((JumboWidth / darabszel) * (darabszel - 1)) >= minWidth)   // Ha csökkentem 1-el a darabszel-t, akkor még a minimum szélesség fölött leszünk-e
                        {
                            iterateAgain = true;
                            darabszel--;
                            JumboWidth = (JumboWidth / (darabszel + 1)) * darabszel;
                        }
                        else // Ekkor visszaáll az eredeti paraméter érték, és a kistekercs hosszát csökkentem (mert eredetileg max értékről indultunk)
                        {
                            iterateAgain = true;
                            JumboWidth = (JumboWidth / (darabszel)) * inputDarabszel;
                            darabszel = inputDarabszel;
                            smallRollLength = smallRollLength - 5; // 5 méterrel csökkentem a kistekercs hosszt és indul újra az iterálás
                        }
                    }
                    else
                        iterateAgain = false;
            //    }


            }
            Jumboproperties.Add("Alap Jumbo hossz", baseJumboLenght);
            Jumboproperties.Add("Alap Jumbo darab", numOfJumboBase);
            Jumboproperties.Add("Alap Jumbo tomeg", baseJumboWeight);
            Jumboproperties.Add("Alap Jumbo hanyszoros", numofSmallRoll);
            Jumboproperties.Add("Extra Jumbo darab", numofJumboExtra);
            Jumboproperties.Add("Extra Jumbo hanyszoros", numofExtraSmallRoll);
            Jumboproperties.Add("Extra Jumbo tömeg", extraJumboWeight);

            inputDarabszel = darabszel;
            inputSmallRollLength = smallRollLength;

            return Jumboproperties;
        }

        /// <summary>
        /// Comerio Jumbo tekercs szélesség meghatározása a kimeneti kistekercs szélesség alapján
        /// </summary>
        /// <param name="foliaWidth"></param>
        /// <param name="maxRep"></param>
        /// <param name="maxWidth"></param>
        /// <returns></returns>
        public int ComerioWidthScale(double foliaWidth, int maxRep, double maxWidth)
        {
            int jumboContainsWidth = 1;
            
            while(jumboContainsWidth <= maxRep && jumboContainsWidth * foliaWidth <= maxWidth)
            {
                jumboContainsWidth += 1;
            }
            if (jumboContainsWidth < 2)
            {
                return 1;
            }
            else
                return jumboContainsWidth - 1;
        }

        /// <summary>
        /// Collect the User Defined Asprova properties
        /// </summary>
        /// <param name="myRootobject"></param>
        /// <returns></returns>
        public Dictionary<string, TPropertyID> GetUserProperties(ASORootObject myRootobject)        // Ebben összeszedjük azokat a user defined propertiket, amik kellenek majd a továbbiakban
        {
            Dictionary<string, TPropertyID> userDefProperties = new Dictionary<string, TPropertyID>();

            //OrderUser_CalcHossz
            ASOPropertyDef propDefCalcHossz = new ASOPropertyDef();
            TPropertyID propertyCalcHosszID = new TPropertyID();
            propDefCalcHossz = myRootobject.LookupPropertyDefFromCode("OrderUser_CalcHossz");
            if (propDefCalcHossz != null)
                propertyCalcHosszID = (TPropertyID)propDefCalcHossz.PropertyID;
            userDefProperties.Add(propDefCalcHossz.Code, propertyCalcHosszID);

            //OrderUser_Calcweight
            ASOPropertyDef propDefCalcWeight = new ASOPropertyDef();
            TPropertyID propertyCalcWeightID = new TPropertyID();
            propDefCalcWeight = myRootobject.LookupPropertyDefFromCode("OrderUser_Calcweight");
            if (propDefCalcWeight != null)
                propertyCalcWeightID = (TPropertyID)propDefCalcWeight.PropertyID;
            userDefProperties.Add(propDefCalcWeight.Code, propertyCalcWeightID);

            // ItemUser_FoliaType
            ASOPropertyDef propDefkiszereles = new ASOPropertyDef();
            TPropertyID propDefkiszerelesID = new TPropertyID();
            propDefkiszereles = myRootobject.LookupPropertyDefFromCode("ItemUser_FoliaType");
            if (propDefkiszereles != null)
                propDefkiszerelesID = (TPropertyID)propDefkiszereles.PropertyID;
            userDefProperties.Add(propDefkiszereles.Code, propDefkiszerelesID);

            // ItemUser_IvLength
            ASOPropertyDef propDefLength = new ASOPropertyDef();
            TPropertyID propDefLengthID = new TPropertyID();
            propDefLength = myRootobject.LookupPropertyDefFromCode("ItemUser_IvLength");
            if (propDefLength != null)
            {
                propDefLengthID = (TPropertyID)propDefLength.PropertyID;
                userDefProperties.Add(propDefLength.Code, propDefLengthID);
            }
            // OrderUser_OrderedTotalL
            ASOPropertyDef orderedTotalLength = new ASOPropertyDef();
            TPropertyID orderedTotalLengthID = new TPropertyID();
            orderedTotalLength = myRootobject.LookupPropertyDefFromCode("OrderUser_OrderedTotalL");
            if (orderedTotalLength != null)
                orderedTotalLengthID = (TPropertyID)orderedTotalLength.PropertyID;
            userDefProperties.Add(orderedTotalLength.Code, orderedTotalLengthID);

            //OrderUser_ComerioDb
            ASOPropertyDef ComerioDb = new ASOPropertyDef();
            TPropertyID ComerioDbID = new TPropertyID();
            ComerioDb = myRootobject.LookupPropertyDefFromCode("OrderUser_ComerioDb");
            if (ComerioDb != null)
                ComerioDbID = (TPropertyID)ComerioDb.PropertyID;
            userDefProperties.Add(ComerioDb.Code, ComerioDbID);

            //OrderUser_ComerioHossz
            ASOPropertyDef ComerioHossz = new ASOPropertyDef();
            TPropertyID ComerioHosszID = new TPropertyID();
            ComerioHossz = myRootobject.LookupPropertyDefFromCode("OrderUser_ComerioHossz");
            if (ComerioHossz != null)
                ComerioHosszID = (TPropertyID)ComerioHossz.PropertyID;
            userDefProperties.Add(ComerioHossz.Code, ComerioHosszID);

            //OrderUser_ComerioKg
            ASOPropertyDef ComerioKg = new ASOPropertyDef();
            TPropertyID ComerioKgID = new TPropertyID();
            ComerioKg = myRootobject.LookupPropertyDefFromCode("OrderUser_ComerioKg");
            if (ComerioKg != null)
                ComerioKgID = (TPropertyID)ComerioKg.PropertyID;
            userDefProperties.Add(ComerioKg.Code, ComerioKgID);

            //OrderUser_ComerioSize
            ASOPropertyDef ComerioSize = new ASOPropertyDef();
            TPropertyID ComerioSizeID = new TPropertyID();
            ComerioSize = myRootobject.LookupPropertyDefFromCode("OrderUser_ComerioSize");
            if (ComerioSize != null)
                ComerioSizeID = (TPropertyID)ComerioSize.PropertyID;
            userDefProperties.Add(ComerioSize.Code, ComerioSizeID);

            //OrderUser_PlugInMaxLength
            ASOPropertyDef PlugInMaxLength = new ASOPropertyDef();
            TPropertyID PlugInMaxLengthID = new TPropertyID();
            PlugInMaxLength = myRootobject.LookupPropertyDefFromCode("OrderUser_PlugInMaxLength");
            if (PlugInMaxLength != null)
            {
                PlugInMaxLengthID = (TPropertyID)PlugInMaxLength.PropertyID;
                userDefProperties.Add(PlugInMaxLength.Code, PlugInMaxLengthID);
            }
            return userDefProperties;
        }

        /// <summary>
        /// Get a user defined Asprova class specified by the classCode
        /// </summary>
        /// <param name="myRootObject"></param>
        /// <param name="classCode"></param>
        /// <returns></returns>
        public ASOClassDef AddUserClass(ASORootObject myRootObject, string classCode)
        {
            Dictionary<string, TClassID> userClasses = new Dictionary<string, TClassID>();

            ASOClassDef userClassDef = new ASOClassDef();
            TClassID userClassID = new TClassID();

            userClassDef = myRootObject.LookupClassDefFromCode(classCode);
            if (userClassDef != null)
                userClassID = (TClassID)userClassDef.ClassID;
            userClasses.Add(userClassDef.Code, userClassID);

            return userClassDef;
        }

        /// <summary>
        /// Get a user defined Asprova property defined by the propertycode parameter
        /// </summary>
        /// <param name="myRootObject"></param>
        /// <param name="propertyCode"></param>
        /// <returns></returns>
        public ASOPropertyDef AddUserProperty(ASORootObject myRootObject, string propertyCode)
        {
            ASOPropertyDef userPropDef = new ASOPropertyDef();
            userPropDef = myRootObject.LookupPropertyDefFromCode(propertyCode);

            return userPropDef;
        }
    }
}

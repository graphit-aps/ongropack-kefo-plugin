﻿using AsLib;
using AsPlugInManager;
using graphIT.Asprova.Plugin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using static AsPlugInManager.TReturnType;

namespace AspOngropackKEFO
{
    public class Command
    {
        public TReturnType CommandMultiPropertyValues(IASPArgList argList)
        {
            //Commands.CommandMultiPropertyValues(argList, new List<string>() { "WorkUser_PrevOperationRecursive", "WorkUser_NextOperationRecursive", "Order_LeftRecursiveOrder", "Order_RightRecursiveOrder", "Work_PrevOperation", "Work_NextOperation" });
            Commands.CommandMultiPropertyValues3(argList, "OperationsPrevious", new List<string>() { "WorkUser_PrevOperationRecursive" });

            

            //Commands.CommandMultiPropertyValues2(argList, new List<string>() { "WorkUser_PrevOperationRecursive", "Order_LeftOrder", "Order_RightOrder", "Work_PrevOperation", "Work_NextOperation" });

            return kContinue;
        }
    }

    
}

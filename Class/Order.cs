﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsLib;

namespace AspOngropackKEFO.Class
{
    [DebuggerDisplay("{Code}")]
    class Order
    {
        public string Code { get; set; }
        public ASBOrderEx Object { get; set; }

        public Item Item { get; set; }
        public double orderQty { get; set; }

        public bool Free { get; set; } = true;

        public bool DisabledFlag { get; set; } = false;
        public bool AssignmentFlag { get; set; }

        public List<Order> NextOrders { get; set; } = new List<Order>();

        public Dictionary<string, double> PeggedQuantities {get;set;}

        public string maxDiameter { get; set; }
        public string maxWeight { get; set; }

        public string FinalUsedMaxSmallRollLength { get; set; }

        public double orderedTotalLength { get; set; }
        public double maxCalcHossz { get; set; }
        public double maxCalcWeight { get; set; }
        public double cseveDiam { get; set; }

        public string ComerioDb { get; set; }
        public string ComerioHossz { get; set; }
        public string ComerioWeight { get; set; }
        public string ComerioSize { get; set; }

        public Order(ASBOrderEx order, Item item)
        {
            Object = order;
            Code = Object.Code;

            Item = item;
            Item.Orders.Add(this);

            PeggedQuantities = new Dictionary<string, double>();
        }
    }
}

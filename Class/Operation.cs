﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsLib;


namespace AspOngropackKEFO.Class
{
    class Operation
    {
        public string Code { get; private set; }

        public ASBOperationEx AsObj { get; private set; }

        public DateTime Est { get; private set; }
        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }

        public Resource MainResource { get; set; }
        public Resource Operator { get; set; }

        public Item MainItem { get; set; }
        
        public List<Operation> prevOperation { get; set; }
        public List<Operation> nextOperation { get; set; }

        public Order OperationOrder { get; set; }
        public List<Order> OperationPrevOrder { get; set; }
        public List<Order> OperationNextOrder { get; set; }

        public Operation(ASBOperationEx asOperation)
        {
            AsObj = asOperation;

            Code = AsObj.Code;
            //Est = AsObj.TotalCalculatedEST;
            StartTime = AsObj.StartTime;
            EndTime = AsObj.EndTime;
        }
    }
}

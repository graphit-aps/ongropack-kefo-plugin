﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsLib;

namespace AspOngropackKEFO.Class
{
    [DebuggerDisplay("{Code} {Component}")]
    class Item
    {
        public string Code { get; set; }
        public ASBItemEx Object { get; set; }

        public List<Order> Orders { get; set; } = new List<Order>();

        public Item Component { get; set; } = null;

        public bool IsComponent { get; set; } = false;

        public double FoliaWidth { get; set; }
        public double FoliaThick { get; set; }
        public double Foliadensity { get; set; }
        public string Foliakiszereles { get; set; }
        public string FoliaLength { get; set; } = null;

        public int CustomLLC { get; set; }

    }
}


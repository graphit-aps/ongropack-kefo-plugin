﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsLib;
using System.Diagnostics;

namespace AspOngropackKEFO.Class
{
    [DebuggerDisplay("{Code}")]
    class Resource
    {
        public ASBResourceEx AsObj { get; private set; }

        public string Code { get; private set; }
        public string Type { get; private set; }
        public TCapacity Capacity { get; private set; }

        public Resource(ASBResource asResource)
        {
            AsObj = (ASBResourceEx)asResource;

            Code = AsObj.Code;
            Capacity = AsObj.ResQtyConstraintMethod == TResQtyConstraintMethod.kResQtyConstraintMethodNoConstraint ? TCapacity.Infinite : TCapacity.Finite;
        }

        public enum TCapacity
        {
            Finite,
            Infinite,
        }

        public enum TType
        {
            Operator,
            Machine,
        }
    }
}

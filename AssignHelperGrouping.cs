﻿using AsLib;
using AsPlugInManager;
using System.Collections.Generic;
using graphIT.Asprova.Plugin;
using System;
using System.Globalization;
using System.Windows.Forms;
using System.Linq;

namespace AspBosal
{
    [Parameter("Grouping")]
    public class AssignHelperGrouping : AssignHelper
    {
        public int counter = 0;
        public Operations operations;
        public Items items;
        public Groups groups;
        public Dictionary<int, Operation> assignedOperations = new Dictionary<int, Operation>();

        public SortedDictionary<int, Operation> needAssign = new SortedDictionary<int, Operation>();
        public List<string> needAssignOperations = new List<string>();
        public List<Group> needAssignGroups = new List<Group>();

        private FormScheduling _progressForm;

        #region "Base"
        public AssignHelperGrouping(Asprova asprova) : base(asprova)
        {
            operations = new Operations(this);
            items = new Items(this);
            groups = new Groups(this);

            /*
            var owner = new Win32Window((IntPtr)asprova.asapp.HWnd);
            _progressForm = new FormScheduling();
            _progressForm.Show(owner);
            _progressForm.SetMax(operations.Count);
            */
        }

        public void Init(ASPArgList argList)
        {
            // After Dispatching
            ASOObjectList operationList = asprova.parentCommands[0].OperationList;

            // Sort list
            if (asprova.parentCommands[0].WorkSortExpression != null)
            {
                operationList.SortByExprStr(asprova.parentCommands[0].WorkSortExpression.GetStr());
            }

            // Set direction for Asprova resource evaluation
            operationList.FilterByExprStr("ME.WorkUser_API_AssignmentDirection = Work_AssignmentDirection");
            operationList.FilterByExprStr("IF(ME.WorkUser_API_AssignmentDirection == 'FF', ME.WorkUser_API_AssignmentDirection = 'F','')");
            operationList.FilterByExprStr("IF(ME.WorkUser_API_AssignmentDirection == 'FB', ME.WorkUser_API_AssignmentDirection = 'B','')");
            operationList.Clear();

            BuildGroups();

            foreach (Group g in groups._internal.Values)
            {
                switch (g.Operations.First().Value.AsOperation.AssignmentDirection)
                {
                    case TWorkAssignmentDirection.kWorkAssignmentDirectionForward:
                    case TWorkAssignmentDirection.kWorkAssignmentDirectionForceForward:
                        g.DirectionOriginal = "F";
                        break;
                    case TWorkAssignmentDirection.kWorkAssignmentDirectionBackward:
                    case TWorkAssignmentDirection.kWorkAssignmentDirectionForceBackward:
                        g.DirectionOriginal = "B";
                        break;
                    default:
                        break;
                }
            }
        }

        public override void End()
        {
            foreach (Operation operation in operations._internal.Values)
            {
                var prop = asprova.getPropertyDef("WorkUser_API_AssignCount");
                if (prop != null)
                {
                    operation.AsOperation.SetAsStr((TPropertyID)prop.PropertyID, 1, operation.CountAssign.ToString());
                }

                prop = asprova.getPropertyDef("WorkUser_API_UnassignCount");
                if (prop != null)
                {
                    operation.AsOperation.SetAsStr((TPropertyID)prop.PropertyID, 1, operation.CountUnassign.ToString());
                }

                prop = asprova.getPropertyDef("WorkUser_API_SimulatedAssignCount");
                if (prop != null)
                {
                    operation.AsOperation.SetAsStr((TPropertyID)prop.PropertyID, 1, operation.CountSimulated.ToString());
                }
            }

            if (_progressForm != null)
            {
                _progressForm.Hide();
                _progressForm.Dispose();
                _progressForm = null;
            }

            counter = 0;
            operations._internal.Clear();
            items._internal.Clear();
            groups._internal.Clear();
            assignedOperations.Clear();

            needAssign.Clear();
            needAssignOperations.Clear();
            needAssignGroups.Clear();
        }
        #endregion

        #region "Asprova events"
        public override void BeforeAssign(ASPArgList argList, ParameterOld param)
        {
            needAssign.Clear();
            needAssignGroups.Clear();
            needAssignOperations.Clear();

            counter++;

            if (_progressForm != null)
            {
                var data = new Dictionary<string, object> {
                            { "count", counter }
                        };
                _progressForm.UpdateProgress(data);
            }

            if (counter == 1) Init(argList);

            var operation = (ASBOperationEx)argList.ArgAsObject[TArgTypeAsObject.kArgOperation];
            operations[operation.Code].AssignmentOrder2 = counter;
        }

        public override void AfterAssign(ASPArgList argList, ParameterOld param)
        {
            var asOperation = (ASBOperationEx)argList.ArgAsObject[TArgTypeAsObject.kArgOperation];
            var operation = operations[asOperation.Code];
            operation.CountAssign += 1;

            var est = (new DateTime[] { asOperation.TotalCalculatedEST, operation.FiniteEST, operation.InfiniteEST }).Max();
            if (est > asOperation.StartTime)
            {
                Correction(operations[asOperation.Code], param);
            }
        }
        #endregion

        public override void RecordOrder(ASPArgList argList, string type)
        {
            if (type == "Forward")
            {
                counter = 0;
                operations._internal.Clear();
                items._internal.Clear();
                groups._internal.Clear();
                assignedOperations.Clear();

                needAssign.Clear();
                needAssignOperations.Clear();
                needAssignGroups.Clear();



                // After Dispatching
                ASOObjectList operationList = asprova.parentCommands[0].OperationList;

                // Sort list
                if (asprova.parentCommands[0].WorkSortExpression != null)
                {
                    operationList.SortByExprStr(asprova.parentCommands[0].WorkSortExpression.GetStr());
                }

                // Store operations
                ASBOperationEx operation;
                for (int i = 1; i <= operationList.ObjectCount; i++)
                {
                    operation = (ASBOperationEx)operationList.Object[i];
                    var o = operations.Add(operation);
                    o.ForwardAssignmentOrder = operation.LastAssignmentOrder;
                }
            }
            else
            {
                foreach (Operation op in operations._internal.Values)
                {
                    if (op.AsOperation.NextOperationCount > 0)
                    {
                        op.AssignmentOrder = op.AsOperation.LastAssignmentOrder;
                    }
                    else
                    {
                        op.AssignmentOrder = 0;
                    }

                    int count = op.AsOperation.PrevOperationCount;
                    for (int i = 1; i <= count; i++)
                    {
                        if (operations.ContainsKey(op.AsOperation.PrevOperation[i].Code))
                        {
                            op.PrevOperations.Add(operations[op.AsOperation.PrevOperation[i].Code]);
                        }
                    }

                    count = op.AsOperation.NextOperationCount;
                    for (int i = 1; i <= count; i++)
                    {
                        if (operations.ContainsKey(op.AsOperation.NextOperation[i].Code))
                        {
                            op.NextOperations.Add(operations[op.AsOperation.NextOperation[i].Code]);
                        }
                    }
                }
            }
        }

        #region "Correction"
        private void Correction(Operation operation, ParameterOld param)
        {
            if (param.Flags.Contains("Single"))
            {
                operation.AsOperation.Unassign();
                assigner.Assign(operation.AsOperation, Assigner.Direction.Forward, operation.InfiniteEST);

                return;
            }

            // Unassign connecting operations
            List<string> processed = new List<string>();
            int minDispatchingOrder = 0;
            UnassignRecursive(operation.AsOperation, ref minDispatchingOrder);
            //UnassignRecursive(operation.AsOperation, ref processed, ref minDispatchingOrder);

            processed.Clear();
            AssignRecursive(operation, ref processed, true);
        }

        #region "Unassign"
        private void UnassignRecursive(ASBOperationEx op, ref int minDispatchingOrder)
        {
            Operation operation = operations[op.Code];

            foreach (Operation o in operation.Group.Operations.Values)
            {
                Unassign(o.AsOperation, ref minDispatchingOrder);
            }

            // Unassign all previous
            if (minDispatchingOrder > 0)
            {
                for (int i = assignedOperations.Count - 1; i <= minDispatchingOrder; i--)
                {
                    Unassign(assignedOperations[i].AsOperation, ref minDispatchingOrder);
                }
            }
        }

        [Obsolete("Use the other one with processed", true)]
        private void UnassignRecursive(ASBOperationEx op, ref List<string> processed, ref int minDispatchingOrder)
        {
            // Deadlock check
            if (processed.Contains(op.Code)) return;
            processed.Add(op.Code);


            // Unassign operations
            if (!Unassign(op, ref minDispatchingOrder)) return;

            // Next operations
            for (int i = op.NextOperationCount; i >= 1; i--)
            {
                UnassignRecursive((ASBOperationEx)op.PrevOperation[i], ref processed, ref minDispatchingOrder);
            }

            // Previous operations
            for (int i = op.PrevOperationCount; i >= 1; i--)
            {
                UnassignRecursive((ASBOperationEx)op.PrevOperation[i], ref processed, ref minDispatchingOrder);
            }

            // Unassign all previous
            if (minDispatchingOrder > 0)
            {
                for (int i = assignedOperations.Count - 1; i <= minDispatchingOrder; i--)
                {
                    Unassign(assignedOperations[i].AsOperation, ref minDispatchingOrder);
                }
            }
        }

        private bool Unassign(ASBOperationEx operation, ref int minDispatchingOrder)
        {
            // Need unassign?
            if (!operations.ContainsKey(operation.Code) || operation.IsAssigned == TIsAssigned.kIsAssignedUnassigned) return false;
            if (needAssignOperations.Contains(operation.Code)) return false;

            operations[operation.Code].CountUnassign += 1;
            operation.Unassign();

            needAssign.Add((int)Math.Max(operations[operation.Code].AssignmentOrder2, 999999999), operations[operation.Code]);
            needAssignOperations.Add(operation.Code);
            if (!needAssignGroups.Contains(operations[operation.Code].Group)) needAssignGroups.Add(operations[operation.Code].Group);

            // Update min dispatching order
            if (minDispatchingOrder == 0 || minDispatchingOrder > operations[operation.Code].AssignmentOrder2)
            {
                minDispatchingOrder = operations[operation.Code].AssignmentOrder2;
            }

            return true;
        }
        #endregion

        #region "Assign"
        private List<Operation> GetAssignmentOperationList()
        {
            return new List<Operation>();
        }

        private void AssignRecursive(Operation operation, ref List<string> processed, bool forward)
        {
            AssigningDirection(operation, ref processed, forward);

            assigner.Assign(operation.AsOperation, forward ? Assigner.Direction.Forward : Assigner.Direction.Backward);

            AssigningDirection(operation, ref processed, !forward);
        }

        public void AssigningDirection(Operation operation, ref List<string> processed, bool forward)
        {
            if (processed.Contains(operation.Code)) return;
            processed.Add(operation.Code);

            ASBOperationEx asop = operation.AsOperation;

            var count = forward ? asop.PrevOperationCount : asop.NextOperationCount;
            for (int i = 1; i <= count; i++)
            {
                ASBOperationEx o = (ASBOperationEx)(forward ? asop.PrevOperation[i] : asop.NextOperation[i]);

                if (operations.ContainsKey(o.Code))
                {
                    if (forward)
                    {
                        AssigningDirection(operations[asop.PrevOperation[i].Code], ref processed, forward);
                        assigner.Assign(o, Assigner.Direction.Forward);
                    }
                    else
                    {
                        assigner.Assign(o, Assigner.Direction.Backward);
                        AssigningDirection(operations[asop.PrevOperation[i].Code], ref processed, forward);
                    }

                }
            }
        }
        #endregion
        #endregion

        #region "Grouping"
        private void BuildGroups()
        {
            List<string> opProcessed = new List<string>();

            // Create groups
            int groupNo = 0;
            foreach (Operation operation in operations._internal.Values)
            {
                if (operation.Group != null) continue;

                // Create new group, Increase next group id
                groupNo++;

                // Register to group
                if (!groups.ContainsKey(groupNo)) groups.Add(groupNo);
                groups[groupNo].AddOperation(operation);

                opProcessed.Add(operation.Code);

                DiscoverOperationPrevNext(groupNo, operation, ref opProcessed);
            }
        }

        private void DiscoverOperationPrevNext(int groupNo, Operation operation, ref List<string> opProcessed)
        {
            foreach (Operation childOperation in operation.PrevOperations)
            {
                if (opProcessed.Contains(childOperation.Code)) continue;
                if (!operations.ContainsKey(childOperation.Code)) continue;

                opProcessed.Add(childOperation.Code);

                groups[groupNo].AddOperation(childOperation);

                DiscoverOperationPrevNext(groupNo, childOperation, ref opProcessed);
            }

            foreach (Operation childOperation in operation.NextOperations)
            {
                if (opProcessed.Contains(childOperation.Code)) continue;
                if (!operations.ContainsKey(childOperation.Code)) continue;

                opProcessed.Add(childOperation.Code);

                groups[groupNo].AddOperation(childOperation);

                DiscoverOperationPrevNext(groupNo, childOperation, ref opProcessed);
            }
        }
        #endregion

        #region "Classes"
        #region "Operation"
        public class Operation
        {
            public Operations parent;

            public List<Operation> PrevOperations { get; set; } = new List<Operation>();
            public List<Operation> NextOperations { get; set; } = new List<Operation>();

            public string Code { get; set; }
            public string Process { get; set; }
            public ASBOperationEx AsOperation { get; set; }
            public Group Group { get; set; }

            public DateTime InfiniteEST { get; set; } = DateTime.MinValue;
            public DateTime InfinitelLET { get; set; } = DateTime.MaxValue;

            public DateTime FiniteEST { get; set; } = DateTime.MinValue;
            public DateTime FinitelLET { get; set; } = DateTime.MaxValue;

            public bool IsWelding { get; set; }
            public Item Item { get; set; }

            public int CountUnassign { get; set; }
            public int CountAssign { get; set; }
            public int CountSimulated { get; set; }


            public int ForwardAssignmentOrder { get; set; } = 0;
            public int AssignmentOrder { get; set; } = 0;

            public int DispathingOrder { get; set; } = 0;
            private int _assignmentOrder = 0;
            public int AssignmentOrder2
            {
                get
                {
                    return _assignmentOrder;
                }
                set
                {
                    parent.parent.assignedOperations.Add(value, this);
                    Group.AssignmentOrder.Add(Code, value);
                }
            }

            public Operation(Operations parent, ASBOperationEx asOperation)
            {
                Code = asOperation.Code;
                Process = asOperation.Master == null ? null : asOperation.Master.Code;
                this.parent = parent;
                AsOperation = asOperation;

                var prop = AssignOld.asprova.getPropertyDef("WorkUser_TheoreticalEST");
                if (prop != null)
                {
                    var value = AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0);
                    if (value != "") InfiniteEST = DateTime.Parse(value, CultureInfo.InvariantCulture);
                }

                prop = AssignOld.asprova.getPropertyDef("WorkUser_TheoreticalLET");
                if (prop != null)
                {
                    var value = AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0);
                    if (value != "") InfinitelLET = DateTime.Parse(value, CultureInfo.InvariantCulture);
                }


                if (AsOperation.OperationMainRes.Code.Contains("WeldingRobot"))
                {
                    IsWelding = true;

                    prop = AssignOld.asprova.getPropertyDef("WorkUser_MainItem");
                    var itemCode = asOperation.GetAsStr((TPropertyID)prop.PropertyID, 1);

                    if (!parent.parent.items.ContainsKey(itemCode))
                    {
                        parent.parent.items.Add(itemCode);
                    }

                    parent.parent.items[itemCode].Update(this);
                }
            }

            public void GetOperationsForAssignment(string direction)
            {
            }
        }

        public class Operations
        {
            public AssignHelperGrouping parent;
            public Dictionary<string, Operation> _internal = new Dictionary<string, Operation>();

            public Operations(AssignHelperGrouping parent)
            {
                this.parent = parent;
            }

            public int Count
            {
                get
                {
                    return _internal.Count;
                }
            }

            public Operation this[string key]
            {
                get
                {
                    return _internal[key];
                }
                set
                {
                    _internal[key] = value;
                }
            }

            public Operation Add(ASBOperationEx operation)
            {
                var code = operation.Code;

                if (!ContainsKey(code))
                {
                    _internal.Add(code, new Operation(this, operation));
                }

                return _internal[code];
            }

            public bool ContainsKey(string code)
            {
                return _internal.ContainsKey(code);
            }
        }
        #endregion

        #region "Item"
        public class Item
        {
            public Items parent;
            public string Code { get; set; }
            public bool NoItems { get; set; }
            public Dictionary<string, Operation> Operations = new Dictionary<string, Operation>();

            public float ManualTime;
            public float AutomaticTime;
            public Dictionary<string, Item> Items = new Dictionary<string, Item>();

            public Item(Items parent, string code)
            {
                this.parent = parent;
                Code = code;
                NoItems = true;
            }

            public void Update(Operation operation)
            {
                if (!Operations.ContainsKey(operation.Code)) Operations.Add(operation.Code, operation);

                if (!NoItems) return;

                NoItems = false;

                CultureInfo culture = CultureInfo.InvariantCulture;

                var prop = AssignOld.asprova.getPropertyDef("WorkUser_WeldingAutomaticTime");
                if (prop != null)
                {
                    var value = operation.AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0).Replace(",", ".");
                    if (value != "") AutomaticTime = float.Parse(value, culture);
                }


                prop = AssignOld.asprova.getPropertyDef("WorkUser_WeldingManualTime");
                if (prop != null)
                {
                    var value = operation.AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0).Replace(",", ".");
                    if (value != "") ManualTime = float.Parse(value, culture);
                }


                prop = AssignOld.asprova.getPropertyDef("WorkUser_WeldingItems");
                var items = operation.AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 0);

                foreach (string item in items.Split(';'))
                {
                    if (item == "") continue;

                    if (!Items.ContainsKey(item))
                    {
                        Items.Add(item, parent.Add(item));
                    }
                }
            }

            public Item(Items parent, Operation operation)
            {
                this.parent = parent;
                var prop = AssignOld.asprova.getPropertyDef("WorkUser_MainItem");
                Code = operation.AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 1);

                Update(operation);
            }
        }

        public class Items
        {
            public AssignHelperGrouping parent;
            public Dictionary<string, Item> _internal = new Dictionary<string, Item>();

            public Items(AssignHelperGrouping parent)
            {
                this.parent = parent;
            }

            public Item Add(string code)
            {
                if (!ContainsKey(code)) _internal.Add(code, new Item(this, code));

                return _internal[code];
            }

            public Item this[string key]
            {
                get
                {
                    return _internal[key];
                }
                set
                {
                    _internal[key] = value;
                }
            }

            public Item Add(Operation operation)
            {
                var prop = AssignOld.asprova.getPropertyDef("WorkUser_MainItem");
                var code = operation.AsOperation.GetAsStr((TPropertyID)prop.PropertyID, 1);

                if (!ContainsKey(code))
                {
                    _internal.Add(code, new Item(this, operation));
                }
                else
                {
                    _internal[code].Update(operation);
                }

                return _internal[code];
            }

            public bool ContainsKey(string code)
            {
                return _internal.ContainsKey(code);
            }
        }
        #endregion

        #region "Group"
        public class Group
        {
            public Groups parent;
            public int Id { get; set; }
            public Dictionary<string, Operation> Operations = new Dictionary<string, Operation>();

            public Dictionary<string, int> DispatchingOrder = new Dictionary<string, int>();
            public Dictionary<string, int> AssignmentOrder = new Dictionary<string, int>();

            public Dictionary<int, Operation> ForwardOrder = new Dictionary<int, Operation>();

            public string DirectionOriginal { get; set; } = null;
            public string DirectionCurrent { get; set; } = null;

            public Group(Groups parent, int id)
            {
                this.parent = parent;
                Id = id;
            }

            public void AddOperation(Operation operation)
            {
                if (!Operations.ContainsKey(operation.Code))
                {
                    Operations.Add(operation.Code, operation);
                    operation.Group = this;
                    DispatchingOrder.Add(operation.Code, operation.DispathingOrder);
                }
            }

            /*public void Reorder(Operation op = null)
            {
                foreach (Operation o in op.NextOperations.Values)
                {

                }

                if (add == false)
                {
                    
                }

                if (add)
                {


                }
            }*/
        }

        public class Groups
        {
            public AssignHelperGrouping parent;
            public Dictionary<int, Group> _internal = new Dictionary<int, Group>();

            public Groups(AssignHelperGrouping parent)
            {
                this.parent = parent;
            }

            public Group Add(int id)
            {
                if (!ContainsKey(id)) _internal.Add(id, new Group(this, id));

                return _internal[id];
            }

            public Group this[int id]
            {
                get
                {
                    return _internal[id];
                }
                set
                {
                    _internal[id] = value;
                }
            }

            public bool ContainsKey(int id)
            {
                return _internal.ContainsKey(id);
            }
        }
        #endregion
        #endregion
    }
}

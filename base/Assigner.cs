﻿using AsLib;
using AsPlugInManager;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace graphIT.Asprova.Plugin
{
    /// <summary>
    /// 
    /// </summary>
    public class Assigner
    {
        Asprova asprova = null;
        ASLAssigner assigner = null;
        ASOPropertyDef propAssignmentDirection;

        public Assigner(Asprova asprova)
        {
            init(ref asprova);
        }

        public Assigner(ref Asprova asprova, ASPSchedulingParameter cmd)
        {
            init(ref asprova);

            // Clone the parameters from the parent scheduling parameter
            var parameter = (ASPSchedulingParameter)assigner.Parameter;
            parameter.AssignmentType = cmd.AssignmentType;
            parameter.AssignmentDirection = cmd.AssignmentDirection;
            parameter.ResSelectionType = cmd.ResSelectionType;
            parameter.IgnoreUnassignedPeggedOperations = TIgnoreUnassignedPeggedOperations.kIgnoreUnassignedPeggedOperationsYes;
            parameter.SuspendExpr = cmd.SuspendExpr;
            parameter.UseCombinationSetup = cmd.UseCombinationSetup;

            // Resource evaluation
            for (int i = 1; i <= cmd.ResEvalCount; i++)
            {
                assigner.Parameter.ResEval[i] = cmd.ResEval[i];
            }

            Marshal.FinalReleaseComObject(parameter);
        }

        private void init(ref Asprova asprova)
        {
            this.asprova = asprova;
            propAssignmentDirection = asprova.getPropertyDef("WorkUser_API_AssignmentDirection", TValueType.kValueTypeSymbol);
            assigner = asprova.asroot.CreateAssigner();
        }

        public void Dispose()
        {
            if (assigner != null)
            {
                // Release assigner, without this Asprova process cannot exit
                Marshal.FinalReleaseComObject(assigner);
                assigner = null;
            }
        }
        

        /// <summary>
        /// Assigns a specific operation
        /// </summary>
        /// <param name="operation">Asprova operation object</param>
        /// <param name="direction">Assignment direction</param>
        /// <param name="time">Reference time for assignment. Forward - EST, Backward - LET</param>
        /// <returns></returns>
        public bool Assign(ASBOperationEx operation, Direction? direction = null, DateTime? time = null)
        {
            /*
            if (operation.Code == "M20170120-05133:20")
            {
                Debug.WriteLine("x");
            }
            */

            bool success = false;
            bool retry = false;

            // Assignment direction
            string directionString = direction == null ? null : Enum.GetName(typeof(Direction), direction);
            var directionOriginal = assigner.Parameter.AssignmentDirection;

            setAssignmentDirection(operation, ref directionString, directionOriginal);

            for (int i = 1; i <= 2; i++)
            {
                // Set the reference time for the command
                if (time == null)
                {
                    time = directionString == "Forward" ? operation.TotalCalculatedEST : operation.TotalCalculatedLET;
                }
                assigner.Parameter.AssignmentStartTime = (directionString == "Forward" ? asprova.createExpression((DateTime)time) : null);
                assigner.Parameter.AssignmentEndTime = (directionString == "Forward" ? null : asprova.createExpression((DateTime)time));

                /* ASSIGN - return if false */
                success = assigner.AssignOperation((ASBOperation)operation, (DateTime)time);
                if (!success) break;

                // Validate the operation assigment, if operation's Start time before TEST then it's not success 
                if (operation.StartTime < operation.TotalCalculatedEST)
                {
                    success = false;
                }


                /* SS timeconstraint */
                if (directionString == "Backward")
                {
                    for (int j = 1; j <= operation.NextOperationCount; j++)
                    {
                        var nextOp = operation.NextOperation[j];

                        // If the next operation is not assigned yet, then exit
                        if (operation.NextOperation[j].IsAssigned != TIsAssigned.kIsAssignedAssigned)
                        {
                            continue;
                        }

                        // Next operation has EST violation?
                        if (operation.NextOperation[j].StartTime < operation.NextOperation[j].TotalCalculatedEST)
                        {
                            // Adjust the reference time for the next assingment
                            time = operation.EndTime - operation.NextOperation[j].TotalCalculatedEST.Subtract(operation.NextOperation[j].StartTime);
                            retry = true;
                            break;
                        }
                    }
                }

                if (!retry) break;
            }

            // restore original direction
            assigner.Parameter.AssignmentDirection = directionOriginal;

            return success;
        }

        /// <summary>
        /// Set assignment firection for Assigner and operation
        /// </summary>
        /// <param name="operation">Asprova Operation object</param>
        /// <param name="directionString">Assignment direction</param>
        /// <param name="directionOriginal">Assigner's original direction</param>
        private void setAssignmentDirection(ASBOperationEx operation, ref string directionString, TAssignmentDirection directionOriginal)
        {
            if (directionString == null)
            {
                // Direction not speficied when called, determine by Asprova parameters
                switch (directionOriginal)
                {
                    case TAssignmentDirection.kCmdAssignmentDirectionForward:
                    case TAssignmentDirection.kCmdAssignmentDirectionForceForward:
                        directionString = "Forward";
                        break;
                    case TAssignmentDirection.kCmdAssignmentDirectionBackward:
                    case TAssignmentDirection.kCmdAssignmentDirectionForceBackward:
                        directionString = "Backward";
                        break;
                    default:
                        // Get direction by order
                        switch (operation.Order.AssignmentDirection)
                        {
                            case TOrderAssignmentDirection.kOrderAssignmentDirectionBackward:
                                directionString = "Backward";
                                break;
                            case TOrderAssignmentDirection.kOrderAssignmentDirectionForward:
                                directionString = "Forward";
                                break;
                            default:
                                if (operation.Order.Priority <= 40 || operation.Order.Priority >= 90)
                                    directionString = "Forward";
                                else
                                    directionString = "Backward";
                                break;
                        }


                        if (operation.Order.LET == DateTime.Parse("#12/30/1899 12:00:00 AM#") && operation.Order.RightmostOrderList.ObjectCount == 0)
                        {
                            directionString = "Forward";
                        }
                        break;
                }
            } else {
                // Dtermined when called, set the Parameters for Assigner
                assigner.Parameter.AssignmentDirection = directionString == "Backward" ? TAssignmentDirection.kCmdAssignmentDirectionBackward : TAssignmentDirection.kCmdAssignmentDirectionForward;
            }

            // Set assignment direction for operation, use user property
            operation.SetAsStr((TPropertyID)propAssignmentDirection.PropertyID, 1, directionString.Substring(0, 1));
        }

        public enum Direction
        {
            Forward,
            Backward
        }

        public class AssignmentDirection
        {
            //private AssignmentDirection(string value) { Value = value; }

            public static string Forward { get { return "F"; } }
        }
    }
}

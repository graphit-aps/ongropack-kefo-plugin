﻿using AsLib;
using AsPlugInManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace graphIT.Asprova.Plugin
{
    [Parameter("")]
    public abstract class AssignHelper : IDisposable
    {
        public Asprova asprova;
        public Assigner assigner;

        public AssignHelper(Asprova asprova)
        {
            this.asprova = asprova;

            assigner = new Assigner(ref asprova, (ASPSchedulingParameter)asprova.parentCommands[0]);
        }

        public abstract void BeforeAssign(ASPArgList argList, ParameterOld param);

        public abstract void AfterAssign(ASPArgList argList, ParameterOld param);

        public abstract void RecordOrder(ASPArgList argList, string type);

        public abstract void End();

        public void Dispose()
        {
            End();

            if (assigner != null)
            {
                // Release assigner, without this Asprova process cannot exit
                assigner.Dispose();
                assigner = null;
            }
        }

        public class Win32Window : IWin32Window
        {
            IntPtr handle;
            public Win32Window(IWin32Window window)
            {
                this.handle = window.Handle;
            }

            public Win32Window(IntPtr handle)
            {
                this.handle = handle;
            }

            IntPtr IWin32Window.Handle
            {
                get { return handle; }
            }
        }

        /// <summary>
        /// Attribute.
        /// </summary>
        [AttributeUsage(AttributeTargets.Class)]
        public class ParameterAttribute : Attribute
        {
            /// <summary>
            /// String field.
            /// </summary>
            string _name;

            /// <summary>
            /// Attribute constructor.
            /// </summary>
            public ParameterAttribute()
            {
            }

            /// <summary>
            /// Attribute constructor.
            /// </summary>
            public ParameterAttribute(string name)
            {
                _name = name;
            }

            /// <summary>
            /// Get and set.
            /// </summary>
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }
        }

        class LongJob
        {
            public void Spin(IProgress<Dictionary<string, object>> progress)
            {
                for (var i = 1; i <= 100; i++)
                {
                    Thread.Sleep(25);
                    if (progress != null)
                    {
                        progress.Report(new Dictionary<string, object> {
                            { "percent", i }
                        });
                    }
                }
            }
        }

        public class ParameterOld
        {
            public string Helper { get; set; }
            public List<string> Flags { get; set; } = new List<string>();
            public Dictionary<string, string> Values { get; set; } = new Dictionary<string, string>();

            public ParameterOld(string str)
            {
                if (!str.Contains(" "))
                {
                    Helper = str;
                }
                else
                {
                    Helper = str.Substring(0, str.IndexOf(" "));
                    str = str.Substring(str.IndexOf(" ") + 1);
                    str = str.Trim(new char[] { ' ', '[', ']' });

                    foreach (string s in str.Split(';'))
                    {
                        if (s.Contains("="))
                        {
                            var s2 = s.Split('=');
                            if (!Values.ContainsKey(s2[0])) Values.Add(s2[0], s2[1]);
                        }
                        else
                        {
                            Flags.Add(s);
                        }
                    }
                }
            }
        }
    }
}

﻿using AsLib;
using AsPlugInManager;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace graphIT.Asprova.Plugin
{
    public class Asprova : IDisposable
    {
        public ASPArgList args;
        public ASORootObject asroot;
        public ASBProject asproj;
        public ASGWorkspace asws;
        public ASFApplication asapp;
        public ASPCommandObject ascmd;
        public string aslang;

        /// <summary>
        /// Constructor, initialize the Asprova default objects
        /// </summary>
        /// <param name="args">Asprova argument list</param>
        public Asprova(ASPArgList args)
        {
            this.args = args;
            asroot = (ASORootObject)args.ArgAsObject[TArgTypeAsObject.kArgRootObject];
            asws = (ASGWorkspace)args.ArgAsObject[TArgTypeAsObject.kArgWorkspace];
            asapp = (ASFApplication)args.ArgAsObject[TArgTypeAsObject.kArgApplication];
            asproj = (ASBProject)args.ArgAsObject[TArgTypeAsObject.kArgProject];
            ascmd = (ASPCommandObject)args.ArgAsObject[TArgTypeAsObject.kArgCommandObject];

            aslang = asproj.FindChild("LocaleDef").Child[1].Code.Substring(0, 2);

            GetParentCommands();
        }

        /// <summary>
        /// Destructor
        /// </summary>
        public void Dispose()
        {

        }

        #region "Property"
        /// <summary>
        /// Gives back the Asprova property object from code. If the type is specified, the property will be created if not exists yet.
        /// </summary>
        /// <param name="code">Property's Asprova code</param>
        /// <param name="type">Asprova type</param>
        /// <returns></returns>
        public ASOPropertyDef getPropertyDef(string code, TValueType? type = null)
        {
            ASOPropertyDef def = asroot.LookupPropertyDefFromCode(code);

            if (def == null && type != null)
            {
                /* Property not exists yet, type specified, create now */
                // Get class name
                var className = code.Substring(0, code.IndexOf("_") - 1).Replace("User", "");

                // create property
                def = asproj.CreateUserPropertyDefWithoutAddingToStyles(asroot.LookupClassDefFromCode(className), code, (TValueType)type, false);
            }

            return def;
        }

        /// <summary>
        /// Get multiple Asprova properties
        /// </summary>
        /// <param name="codes">Collection of properties. The dictionary value can be null, in that case the missing property not created</param>
        /// <returns></returns>
        public Dictionary<string, ASOPropertyDef> getPropertyDefs(Dictionary<string, TValueType?> codes)
        {
            var res = new Dictionary<string, ASOPropertyDef>();

            foreach (KeyValuePair<string, TValueType?> pair in codes)
            {
                ASOPropertyDef tempDef = getPropertyDef(pair.Key, pair.Value);

                if (tempDef == null) continue;

                res.Add(pair.Key, tempDef);
            }

            return res;
        }
        #endregion

        #region "Expression"
        public ASVExpression createExpression(string text)
        {
            ASVExpression expr = asroot.CreateExpression();
            expr.SetStr(text);

            return expr;
        }

        public ASVExpression createExpression(DateTime time)
        {
            ASVExpression expr = asroot.CreateExpression();
            expr.SetStr("#" + time.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture) + "#");

            return expr;
        }
        #endregion

        #region "DBIO"
        public bool executeImport(Dictionary<string, TIsLoadTable> targets)
        {
            return executeDBIO(targets, null);
        }

        public bool executeExport(Dictionary<string, TIsSaveTable> targets)
        {
            return executeDBIO(null, targets);
        }

        private bool executeDBIO(Dictionary<string, TIsLoadTable> importTargets, Dictionary<string, TIsSaveTable> exportTargets)
        {
            try
            {
                ASIDBIO dbio;
                List<ASIDBIO> dbios = new List<ASIDBIO>();
                ASPCommandObject rootCommand = asproj.RootCommandObject;
                ASPImport cmdImport = null;
                ASPExport cmdExport = null;

                // Delete all previous command temp
                bool deleted = false;
                int i = 1;
                do
                {
                    deleted = false;
                    for (i = 1; i <= rootCommand.ChildCount; i++)
                    {
                        if (rootCommand.Child[i].Code == "CmdTemp")
                        {
                            rootCommand.DeleteChild(i);
                            deleted = true;
                            continue;
                        }
                    }
                } while (deleted);

                // Command
                if (importTargets != null)
                {
                    cmdImport = (ASPImport)rootCommand.AddChildAsCommand(TCmdClassID.kCmdASPImport, "CmdTemp");

                    //Add dbio object to import command
                    foreach (KeyValuePair<string, TIsLoadTable> pair in importTargets)
                    {
                        dbio = (ASIDBIO)asproj.RootDBIO.FindChild(pair.Key);
                        if (dbio == null) continue;

                        dbios.Add(dbio);

                        cmdImport.Import_DBIO[0] = dbio;
                        dbio.IsLoadTable = pair.Value;
                    }

                    // Execute
                    cmdImport.Execute((ASPCommandObject)cmdImport);
                }
                else
                {
                    cmdExport = (ASPExport)rootCommand.AddChildAsCommand(TCmdClassID.kCmdASPExport, "CmdTemp");

                    //Add dbio object to import command
                    foreach (KeyValuePair<string, TIsSaveTable> pair in exportTargets)
                    {
                        dbio = (ASIDBIO)asproj.RootDBIO.FindChild(pair.Key);
                        if (dbio == null) continue;

                        dbios.Add(dbio);

                        cmdExport.Export_DBIO[0] = dbio;
                        dbio.IsSaveTable = pair.Value;
                    }

                    // Execute
                    cmdExport.Execute((ASPCommandObject)cmdExport);
                }

                // Delete command
                for (i = 1; i <= rootCommand.ChildCount; i++)
                {
                    if (rootCommand.Child[i].Code == "CmdTemp")
                    {
                        rootCommand.DeleteChild(i);
                        break;
                    }
                }

                // Set the import and export to no
                foreach (ASIDBIO d in dbios)
                {
                    if (d == null) continue;

                    if (importTargets != null)
                        d.IsLoadTable = TIsLoadTable.kIsLoadTableNo;
                    else
                        d.IsSaveTable = TIsSaveTable.kIsSaveTableNo;
                }

                // Check that there was any error
                foreach (ASIDBIO d in dbios)
                {
                    if (d == null) continue;

                    if (d.ErrorCodeCount > 0)
                    {
                        return false;
                    }
                }

                return true;
            } catch
            {

            }

            return false;
        }
        #endregion

        #region "Check"
        public bool CheckPluginVersion(string version = null)
        {
            if (version == null)
            {
                version = Assembly.GetEntryAssembly().GetName().Version.ToString();
                version = version.Substring(0, version.IndexOf('.', version.IndexOf('.') + 1));
            } 

            if (asproj.GetAsStr((TPropertyID)getPropertyDef("ProjectUser_PluginVersion", TValueType.kValueTypeSymbol).PropertyID,1) != version)
            {
                //core.CreateLogEntry(string.Format("Plugin version problem ({0} - {1})", GetAspPropertyValue(args, "ProjectUser_PluginVersion"), version));
                //MessageBox.Show(string.Format("Plugin verzió nem megfelelő {0} - {1}", GetAspPropertyValue(args, "ProjectUser_PluginVersion"), version), "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        public bool CheckPath()
        {
            string mainpath = asproj.GetAsStr((TPropertyID)getPropertyDef("ProjectUser_PluginVersion", TValueType.kValueTypeSymbol).PropertyID, 1);
            string aspath = Utils.GetUNCPath(asws.ProjectPathName + "\\" + asws.ProjectFilename);
            asproj = null;

            if (mainpath.ToLower() != aspath.ToLower())
            {
                //core.CreateLogEntry("Prototype (" + aspath.ToLower + ") is not the main prototype (" + mainpath.ToLower + ")!");
                //MessageBox.Show("A jelenleg megnyitott prototípus (" + aspath.ToLower + ") nem a fő prototípus (" + mainpath.ToLower + ")!", "Prototípus hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }


        public bool CheckLicense()
        {
            if (asapp.IsValidLicense)
            {
                return true;
            }
            else
            {
                //core.CreateLogEntry("License problem");
                //MessageBox.Show("Licenc probléma", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return false;
        }

        public int checkOperationCount(string exprFilter)
        {
            ASOObjectList operationList = asproj.GetOperationList(true);

            int count = 0;

            // Count operations
            operationList.FilterByExprStr(exprFilter);
            count = operationList.ObjectCount;
            operationList.Clear();

            return count;
        }
        #endregion

        #region "Command objects"
        public List<ASPDefaultSchedulingParameter> parentCommands = new List<ASPDefaultSchedulingParameter>();

        public void GetParentCommands()
        {
            parentCommands.Clear();
            parentCommands.Add((ASPDefaultSchedulingParameter)ascmd.Parent);

            GetParentCommandsRecursive(parentCommands[0]);
        }

        private void GetParentCommandsRecursive(ASPDefaultSchedulingParameter cmd)
        {
            try
            {
                if (cmd.Parent.Code == "Scheduling") return;
                parentCommands.Add((ASPDefaultSchedulingParameter)cmd.Parent);
                GetParentCommandsRecursive(parentCommands[parentCommands.Count - 1]);
            } catch { }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace graphIT.Asprova.Plugin
{
    public partial class FormScheduling : Form
    {
        private int max;

        public FormScheduling()
        {
            InitializeComponent();
        }

        private void FormScheduling_Load(object sender, EventArgs e)
        {
        }

        public void SetMax(int value)
        {
            max = value;
            pbProgress.Maximum = 100;
        }

        public void UpdateProgress(Dictionary<string, object> data)
        {
            if ((int)data["count"] * 100 / max > pbProgress.Value)
            {
                pbProgress.Value = (int)data["count"] * 100 / max;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsLib;
using AsPlugInManager;

namespace AspOngropackKEFO
{
    public static class Extensions
    {
        public static Dictionary<string, TPropertyID?> propertyIds = new Dictionary<string, TPropertyID?>();

        public static string GetUntil(this string text, string stopAt = "_")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return text;
        }

        #region "Asprova"
        public static int? Priority(this ASBMasterInputInstruction instMaster)
        {
            var value = GetPropertyValue("InputBomInstructionUser_Priority", (ASOObject)instMaster);
            return value == null || value == "" ? null : (int?)Convert.ToInt32(value);
        }

        #region "Project"
        public static string DynamicPurchasePegging(this ASBProjectEx proj)
        {
            return GetPropertyValue("ProjectUser_DynamicPurchasePegging", (ASOObject)proj);
        }

        public static string ApiError(this ASBProject proj, string value = null)
        {
            var propCode = "ProjectUser_ApiError";

            if (value == null)
            {
                value = GetPropertyValue(propCode, (ASOObject)proj);
                value = value == null || value == "" ? null : value;
            }
            else
            {
                SetPropertyValue(propCode, (ASOObject)proj, value);
            }

            return value;
        }

        public static bool SilentMode(this ASBProject proj, bool? value = null)
        {
            var propCode = "ProjectUser_SilentMode";

            if (value == null)
            {
                value = GetPropertyValue(propCode, (ASOObject)proj) == "1";
            }
            else
            {
                SetPropertyValue(propCode, (ASOObject)proj, (bool)value ? "1" : "0");
            }

            return (bool)value;
        }

        public static int ScheduleCountByDay(this ASBProject proj)
        {
            var propCode = "ProjectUser_ScheduleCountByDay";

            return int.Parse(GetPropertyValue(propCode, (ASOObject)proj));
        }
        public static int ScheduleCountByDay(this ASBProjectEx proj)
        {
            return ScheduleCountByDay((ASBProject)proj);
        }

        public static DateTime LastSchedulingTime(this ASBProject proj)
        {
            return DateTime.Parse(GetPropertyValue("ProjectUser_LastSchedulingTime", (ASOObject)proj));
        }
        public static DateTime LastSchedulingTime(this ASBProjectEx proj)
        {
            return LastSchedulingTime((ASBProject)proj);
        }

        #endregion

        #region "Resource"
        public static int? Qty(this ASBResource asObj)
        {
            return GetPropertyValue("ResourceUser_Qty", (ASOObject)asObj).Ext(v => v == "" ? (int?)null : (int?)int.Parse(v));
        }
        public static int? Qty(this ASBResourceEx asObj)
        {
            return Qty((ASBResource)asObj);
        }

        public static string OperatorCode(this ASBResource asObj)
        {
            return GetPropertyValue("ResourceUser_OperatorCode", (ASOObject)asObj).Ext(v => v == "" ? null : v);
        }
        public static string OperatorCode(this ASBResourceEx asObj)
        {
            return OperatorCode((ASBResource)asObj);
        }

        public static int? ShiftPerDay(this ASBResource asObj)
        {
            return GetPropertyValue("ResourceUser_MuszakPerNap", (ASOObject)asObj).Ext(v => v == "" ? (int?)null : (int?)int.Parse(v));
        }
        public static int? ShiftPerDay(this ASBResourceEx asObj)
        {
            return ShiftPerDay((ASBResource)asObj);
        }
        #endregion

        #region "Calendar"
        public static IEnumerable<string> _Dates(this ASBCalendar asObj)
        {
            for (int i = 1; i <= asObj.DateCount; i++)
                yield return asObj.Date[i].GetStr();
        }

        public static IEnumerable<string> _Resources(this ASBCalendar asObj)
        {
            for (int i = 1; i <= asObj.ResourceCount; i++)
                yield return asObj.Resource[i].Code;
        }

        public static IEnumerable<string> _Shifts(this ASBCalendar asObj)
        {
            for (int i = 1; i <= asObj.ShiftCodeCount; i++)
                yield return asObj.ShiftCode[i];
        }

        public static double SortOrder2(this ASBCalendar asObj)
        {
            return Convert.ToDouble((GetPropertyValue("CalendarUser_SortOrder2", (ASOObject)asObj) ?? "0").Replace(",", "."));
        }

        public static string ValidPeriod(this ASBCalendar asObj)
        {
            return GetPropertyValue("CalendarUser_ValidPeriod", (ASOObject)asObj).Ext(v => v);
        }

        public static string Custom(this ASBCalendar asObj)
        {
            return GetPropertyValue("CalendarUser_Custom", (ASOObject)asObj).Ext(v => v);
        }

        public static void Custom(this ASBCalendar asObj, string value)
        {
            SetPropertyValue("CalendarUser_Custom", (ASOObject)asObj, value);
        }

        public static void SetResource(this ASBCalendar asObj, string value)
        {
            SetPropertyValue("Cal_Resource", (ASOObject)asObj, value);
        }

        public static void SetDate(this ASBCalendar asObj, string value)
        {
            SetPropertyValue("Cal_Dates", (ASOObject)asObj, value);
        }

        public static void SetShiftCodes(this ASBCalendar asObj, string value)
        {
            SetPropertyValue("Cal_ShiftCode", (ASOObject)asObj, value);
        }

        public static void SetShiftCode(this ASBCalendar asObj, string value, int index)
        {
            SetPropertyValue("Cal_ShiftCode", (ASOObject)asObj, value, index);
        }
        #endregion

        #region "Item"
        public static bool HasQuality(this ASBItemEx item)
        {
            var value = GetPropertyValue("ItemUser_HasQuality", (ASOObject)item);
            return value == null || value == "" ? false : value == "1";
        }

        public static bool IsAlternate(this ASBItemEx item)
        {
            var value = GetPropertyValue("ItemUser_IsAlternate", (ASOObject)item);
            return value == null || value == "" ? false : value == "1";
        }

        public static int? ExplodeNow(this ASBItemEx item)
        {
            return ExplodeNowPrivate(item as ASBItem);
        }

        public static int? ExplodeNow(this ASBItem item)
        {
            return ExplodeNowPrivate(item);
        }

        public static void ExplodeNow(this ASBItemEx item, int value)
        {
            SetPropertyValue("ItemUser_ExplodeNow", (ASOObject)item, value.ToString());
        }

        private static int? ExplodeNowPrivate(ASBItem item)
        {
            var value = GetPropertyValue("ItemUser_ExplodeNow", (ASOObject)item);
            return value == null || value == "" ? null : (int?)Convert.ToInt32(value);
        }

        public static bool HasAlternateInput(this ASBItemEx item)
        {
            var value = GetPropertyValue("ItemUser_HasAlternateInput", (ASOObject)item);
            return value == "1";
        }
        #endregion

        #region "Order"
        public static bool Confirmed(this ASBOrder order)
        {
            var value = GetPropertyValue("OrderUser_Confirmed", (ASOObject)order);
            return value == null || value == "" ? false : (value == "1");
        }
        public static bool Confirmed(this ASBOrderEx order)
        {
            return Confirmed((ASBOrder)order);
        }

        #region OrderUser_Customer2
        public static string Customer2(this ASBOrder order)
        {
            return GetPropertyValue("OrderUser_Customer2", (ASOObject)order);
        }
        public static string Customer2(this ASBOrderEx order)
        {
            return Customer2((ASBOrder)order);
        }
        #endregion



        private static int? FirstProcessNumber(ASBOrder asObj, int? value = null)
        {
            var prop = "OrderUser_FirstProcessNumber";

            if (value != null)
            {
                SetPropertyValue(prop, (ASOObject)asObj, value.ToString());
                return value;
            }

            var asValue = GetPropertyValue(prop, (ASOObject)asObj);
            return asValue == null || asValue == "" ? null : (int?)Convert.ToInt32(value);
        }
        public static int? FirstProcessNumber(this ASBOrderEx asObj, int? value = null)
        {
            return FirstProcessNumber((ASBOrder)asObj, value);
        }

        public static string Quality(this ASBOrder order)
        {
            var value = GetPropertyValue("OrderUser_Quality", (ASOObject)order);
            return value == null || value == "" ? null : value;
        }
        public static string Quality(this ASBOrderEx asObj)
        {
            return Quality((ASBOrder)asObj);
        }

        public static string QualityInstruction(this ASBOrder asObj)
        {
            var value = GetPropertyValue("OrderUser_QualityInstruction", (ASOObject)asObj);
            return value == null || value == "" ? null : value;
        }
        public static string QualityInstruction(this ASBOrderEx asObj)
        {
            return QualityInstruction((ASBOrder)asObj);
        }

        public static void QualityInstruction(this ASBOrder asObj, string value)
        {
            SetPropertyValue("OrderUser_QualityInstruction", (ASOObject)asObj, value);
        }
        public static void QualityInstruction(this ASBOrderEx asObj, string value)
        {
            QualityInstruction((ASBOrder)asObj, value);
        }
        #endregion

        #region "Operation"
        public static int? API_DispatchingOrderForward(this ASBOperationEx operation)
        {
            var value = GetPropertyValue("WorkUser_API_DispatchingOrderForward", (ASOObject)operation);
            return value == null || value == "" ? null : (int?)Convert.ToInt32(value);
        }

        public static int? ResourceQtyMin(this ASBOperationEx operation)
        {
            var value = GetPropertyValue("WorkUser_ResourceQtyMin", (ASOObject)operation);
            return value == null || value == "" ? null : (int?)Convert.ToInt32(value);
        }

        public static int? ResourceQtyMax(this ASBOperationEx operation)
        {
            var value = GetPropertyValue("WorkUser_ResourceQtyMax", (ASOObject)operation);
            return value == null || value == "" ? null : (int?)Convert.ToInt32(value);
        }

        public static int? RequiredResQty(this ASBOperationEx operation)
        {
            var prop = "WorkUser_RequiredResQty";

            var value = GetPropertyValue(prop, (ASOObject)operation);
            return value == null || value == "" ? null : (int?)Convert.ToInt32(value);
        }

        public static int? RequiredResQty(this ASBOperationEx operation, int? resourceQty)
        {
            var prop = "WorkUser_RequiredResQty";
            SetPropertyValue(prop, (ASOObject)operation, resourceQty.ToString().Replace(",", "."));
            return resourceQty;
        }

        public static void ApiLateness(this ASBOperationEx operation, int value)
        {
            SetPropertyValue("WorkUser_ApiLateness", (ASOObject)operation, value == 0 ? "" : value.ToString());
        }

        public static void ForwardStartTime(this ASBOperationEx operation, DateTime time)
        {
            SetPropertyValue("WorkUser_ForwardStartTime", (ASOObject)operation, time.ToString("yyyy'/'MM'/'dd HH:mm:ss"));
        }

        public static void ForwardEst(this ASBOperationEx operation, DateTime time)
        {
            SetPropertyValue("WorkUser_ForwardEst", (ASOObject)operation, time.ToString("yyyy'/'MM'/'dd HH:mm:ss"));
        }

        public static void ApiAssignGroup(this ASBOperationEx operation, int value)
        {
            SetPropertyValue("WorkUser_ApiAssignGroup", (ASOObject)operation, value.ToString());
        }

        public static void ApiAssignOrder(this ASBOperationEx operation, int value)
        {
            SetPropertyValue("WorkUser_ApiAssignOrder", (ASOObject)operation, value.ToString());
        }

        public static void ApiAssignmentDirection(this ASBOperationEx operation, string value)
        {
            SetPropertyValue("WorkUser_API_AssignmentDirection", (ASOObject)operation, value);
        }



        #endregion

        #region "Asprova Helper"
        public static string GetPropertyValueAll(string propertyCode, ASOObject obj)
        {
            var proj = obj.ClassDef.Parent.Parent as ASBProjectEx;
            try
            {
                if (!propertyIds.ContainsKey(propertyCode))
                {
                    propertyIds.Add(propertyCode, (TPropertyID?)proj.RootObject.LookupPropertyDefFromCode(propertyCode)?.PropertyID);

                }

                if (propertyIds[propertyCode] == null)
                    return null;

                var count = proj.RootObject.PropertyValueCount(obj, (int)propertyIds[propertyCode]);

                var values = new List<string>();
                for (int i = 1; i <= count; i++)
                {
                    values.Add(obj.GetAsStr((TPropertyID)propertyIds[propertyCode], i));
                }

                return string.Join(";", values);
            }
            finally
            {
                //Helper.Release(proj);
            }
        }

        public static string GetPropertyValue(string propertyCode, ASOObject obj)
        {
            if (!propertyIds.ContainsKey(propertyCode))
            {
                var proj = obj.ClassDef.Parent.Parent as ASBProjectEx;
                propertyIds.Add(propertyCode, (TPropertyID?)proj.RootObject.LookupPropertyDefFromCode(propertyCode)?.PropertyID);
                //Helper.Release(proj);
            }

            if (propertyIds[propertyCode] == null)
                return null;

            return obj.GetAsStr((TPropertyID)propertyIds[propertyCode], 1);
        }

        public static void SetPropertyValue(string propertyCode, ASOObject obj, string value, int index = 1)
        {
            if (!propertyIds.ContainsKey(propertyCode))
            {
                var proj = obj.ClassDef.Parent.Parent as ASBProjectEx;
                propertyIds.Add(propertyCode, (TPropertyID?)proj.RootObject.LookupPropertyDefFromCode(propertyCode)?.PropertyID);
                //Helper.Release(proj);
            }

            if (propertyIds[propertyCode] == null)
                return;

            obj.SetAsStr((TPropertyID)propertyIds[propertyCode], index, value);
        }
        #endregion
        #endregion


        #region Asprova
        public static IEnumerable<T> _ForEach<T>(this ASOObjectList obj)
        {
            var count = obj.ObjectCount;
            for (int i = 1; i <= count; i++)
            {
                yield return (T)obj.Object[i];
            }
        }

        #region Objs
        public static IEnumerable<T> _Objs<T>(this ASBProjectEx proj, string objName)
        {
            var root = proj.FindChild(objName);
            var count = root.ChildCount;
            for (int i = 1; i <= count; i++)
                yield return (T)root.Child[i];
        }
        #endregion

        #region Calendar
        public static IEnumerable<ASBCalendar> _Calendars(this ASBProject proj)
        {
            var root = proj.RootCalendar;
            var count = root.ChildCount;
            for (int i = 1; i <= count; i++)
            {
                yield return root.ChildAsCalendar[i];
            }
        }

        public static IEnumerable<ASBCalendar> _Calendars(this ASBProjectEx proj)
        {
            return _Calendars((ASBProject)proj);
        }
        #endregion

        #region Items
        public static IEnumerable<T> _Items<T>(this ASBProjectEx proj)
        {
            var root = proj.RootItem;
            var count = root.ChildCount;
            for (int i = 1; i <= count; i++)
            {
                yield return (T)root.ChildAsItem[i];
            }
        }
        #endregion

        #region Operation
        #region InputInstructionListRecursive
        public static IEnumerable<ASBInputInstruction> _InputInstructionListRecursive(this ASBOperation op)
        {
            var objs = op.InputInstructionListRecursive;
            var count = objs.ObjectCount;
            for (int i = 1; i <= count; i++)
            {
                yield return (ASBInputInstruction)objs.Object[i];
            }
        }
        public static IEnumerable<ASBInputInstruction> _InputInstructionListRecursive(this ASBOperationEx op)
        {
            return _InputInstructionListRecursive((ASBOperation)op);
        }
        #endregion

        #region OutputInstructionListRecursive
        public static IEnumerable<ASBOutputInstruction> _OutputInstructionListRecursive(this ASBOperation op)
        {
            var objs = op.OutputInstructionListRecursive;
            var count = objs.ObjectCount;
            for (int i = 1; i <= count; i++)
            {
                yield return (ASBOutputInstruction)objs.Object[i];
            }
        }
        public static IEnumerable<ASBOutputInstruction> _OutputInstructionListRecursive(this ASBOperationEx op)
        {
            return _OutputInstructionListRecursive((ASBOperation)op);
        }
        #endregion

        #region PrevOperation
        public static IEnumerable<T> _PrevOperation<T>(this ASBOperation op)
        {
            var count = op.PrevOperationCount;
            for (int i = 1; i <= count; i++)
            {
                yield return (T)op.PrevOperation[i];
            }
        }
        public static IEnumerable<T> _PrevOperation<T>(this ASBOperationEx op)
        {
            return _PrevOperation<T>((ASBOperation)op);
        }
        #endregion

        #region ApiLatePurchaseOrders
        private static readonly string propWorkUserApiLatePurchaseOrders = "WorkUser_ApiLatePurchaseOrders";
        public static string ApiLatePurchaseOrders(this ASBOperation op)
        {
            return GetPropertyValue(propWorkUserApiLatePurchaseOrders, (ASOObject)op);
        }
        public static string ApiLatePurchaseOrders(this ASBOperationEx op)
        {
            return ApiLatePurchaseOrders((ASBOperation)op);
        }

        public static void ApiLatePurchaseOrders(this ASBOperation op, string value)
        {
            SetPropertyValue(propWorkUserApiLatePurchaseOrders, (ASOObject)op, value);
        }
        public static void ApiLatePurchaseOrders(this ASBOperationEx op, string value)
        {
            ApiLatePurchaseOrders((ASBOperation)op, value);
        }
        #endregion

        #region ApiLateCapacityInfo
        private static readonly string propWorkUserApiLateCapacityInfo = "WorkUser_ApiLateCapacityInfo";
        public static string ApiLateCapacityInfo(this ASBOperation obj)
        {
            return GetPropertyValue(propWorkUserApiLateCapacityInfo, (ASOObject)obj);
        }
        public static string ApiLateCapacityInfo(this ASBOperationEx obj)
        {
            return ApiLateCapacityInfo((ASBOperation)obj);
        }

        public static void ApiLateCapacityInfo(this ASBOperation obj, string value)
        {
            SetPropertyValue(propWorkUserApiLateCapacityInfo, (ASOObject)obj, value);
        }
        public static void ApiLateCapacityInfo(this ASBOperationEx obj, string value)
        {
            ApiLateCapacityInfo((ASBOperation)obj, value);
        }
        #endregion
        #endregion

        #region Order
        #region CustomOrderType
        public static string CustomOrderType(this ASBOrder order)
        {
            return GetPropertyValue("OrderUser_CustomOrderType", (ASOObject)order);
        }
        public static string CustomOrderType(this ASBOrderEx order)
        {
            return CustomOrderType((ASBOrder)order);
        }
        #endregion

        #region ErpType 
        // BESZERZÉSI PROTOTÍPUSBAN CSAK!
        // 0,S,Sales,,;1,F,Forecast,,;2,I,Készlet,,;3,P,Beszerzés,,;4,O,Kiszervezett,,;5,M,Gyártás,,
        public static string ErpType(this ASBOrder order)
        {
            return GetPropertyValue("OrderUser_ErpType", (ASOObject)order);
        }
        public static string ErpType(this ASBOrderEx order)
        {
            return ErpType((ASBOrder)order);
        }
        #endregion

        #region Raktar 
        // BESZERZÉSI PROTOTÍPUSBAN CSAK!
        public static string Raktar(this ASBOrder order)
        {
            return GetPropertyValue("OrderUser_Raktar", (ASOObject)order);
        }
        public static string Raktar(this ASBOrderEx order)
        {
            return Raktar((ASBOrder)order);
        }
        #endregion

        #region PriorityCustom
        public static int PriorityCustom(this ASBOrder order)
        {
            return Convert.ToInt32(GetPropertyValue("OrderUser_PriorityCustom", (ASOObject)order));
        }
        public static int PriorityCustom(this ASBOrderEx order)
        {
            return PriorityCustom((ASBOrder)order);
        }
        #endregion

        #region _RightmostOrderList
        public static IEnumerable<T> _RightmostOrderList<T>(this ASBOrder order)
        {
            var count = order.RightmostOrderList.ObjectCount;
            for (int i = 1; i <= count; i++)
            {
                yield return (T)order.RightmostOrderList.Object[i];
            }
        }
        public static IEnumerable<T> _RightmostOrderList<T>(this ASBOrderEx order)
        {
            return _RightmostOrderList<T>((ASBOrder)order);
        }
        #endregion

        #region ApiCapacityLateness
        private static readonly string propOrderUserApiCapacityLateness = "OrderUser_ApiCapacityLateness";
        public static string ApiCapacityLateness(this ASBOrder order)
        {
            return GetPropertyValue(propOrderUserApiCapacityLateness, (ASOObject)order);
        }
        public static string ApiCapacityLateness(this ASBOrderEx order)
        {
            return ApiCapacityLateness((ASBOrder)order);
        }

        public static void ApiCapacityLateness(this ASBOrder order, int? value)
        {
            SetPropertyValue(propOrderUserApiCapacityLateness, (ASOObject)order, value == null ? "" : value.ToString());
        }
        public static void ApiCapacityLateness(this ASBOrderEx order, int? value)
        {
            ApiCapacityLateness((ASBOrder)order, value);
        }
        #endregion

        #region ApiPurchaseLateness
        private static readonly string propOrderUserApiPurhcaseLateness = "OrderUser_ApiPurchaseLateness";
        public static string ApiPurchaseLateness(this ASBOrder order)
        {
            return GetPropertyValue(propOrderUserApiPurhcaseLateness, (ASOObject)order);
        }
        public static string ApiPurchaseLateness(this ASBOrderEx order)
        {
            return ApiPurchaseLateness((ASBOrder)order);
        }

        public static void ApiPurchaseLateness(this ASBOrder order, int? value)
        {
            SetPropertyValue(propOrderUserApiPurhcaseLateness, (ASOObject)order, value == null ? "" : value.ToString());
        }
        public static void ApiPurchaseLateness(this ASBOrderEx order, int? value)
        {
            ApiPurchaseLateness((ASBOrder)order, value);
        }
        #endregion

        #region KesesKapacitasInfo
        private static readonly string propOrderUserKesesKapacitasInfo = "OrderUser_KesesKapacitasInfo";
        public static string KesesKapacitasInfo(this ASBOrder order)
        {
            return GetPropertyValue(propOrderUserKesesKapacitasInfo, (ASOObject)order);
        }
        public static string KesesKapacitasInfo(this ASBOrderEx order)
        {
            return KesesKapacitasInfo((ASBOrder)order);
        }

        public static void KesesKapacitasInfo(this ASBOrder order, string value)
        {
            SetPropertyValue(propOrderUserKesesKapacitasInfo, (ASOObject)order, value);
        }
        public static void KesesKapacitasInfo(this ASBOrderEx order, string value)
        {
            KesesKapacitasInfo((ASBOrder)order, value);
        }
        #endregion
        #endregion

        #region Item
        public static IEnumerable<T> _OrderList<T>(this ASBItemEx asItem)
        {
            var container = asItem.OrderList;
            var count = container.ObjectCount;
            for (int i = 1; i <= count; i++)
            {
                yield return (T)container.Object[i];
            }
        }

        public static int PurchaseLeadTime2(this ASBItemEx asObj)
        {
            var value = GetPropertyValue("ItemUser_PurchaseLeadTime2", (ASOObject)asObj);
            return ConvertTime(value);
        }


        #endregion

        #region Task
        #region _UseInstrucion
        public static IEnumerable<ASBUseInstruction> _UseInstrucion(this ASBTask obj)
        {
            var count = obj.UseInstructionCount;
            for (int i = 1; i <= count; i++)
            {
                yield return obj.UseInstruction[i];
            }
        }
        #endregion
        #endregion

        #region InputInstruction
        #region _Pegs
        public static IEnumerable<ASBPeg> _Pegs(this ASBInputInstruction obj)
        {
            var count = obj.PegCount;
            for (int i = 1; i <= count; i++)
            {
                yield return obj.Peg[i];
            }
        }
        #endregion
        #endregion

        #region OutputInstruction
        #region _Pegs
        public static IEnumerable<ASBPeg> _Pegs(this ASBOutputInstruction obj)
        {
            var count = obj.PegCount;
            for (int i = 1; i <= count; i++)
            {
                yield return obj.Peg[i];
            }
        }
        #endregion
        #endregion
        #endregion

        private static int ConvertTime(string valueStr)
        {
            if ((valueStr ?? "").Trim() == "")
                return 0;

            var unit = valueStr.Substring(valueStr.Length - 1, 1).ToLower();
            var val = Convert.ToInt32(valueStr.Substring(0, valueStr.Length - 1));

            switch (unit)
            {
                case "s":
                    return val;
                case "h":
                    return val * 60 * 60;
                case "d":
                    return val * 24 * 60 * 60;
                case "w":
                    return val * 7 * 24 * 60 * 60;
                case "m":
                default:
                    return val * 60;
            }
        }
    }
}
